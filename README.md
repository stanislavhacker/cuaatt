![CUAATT][logo] CUAATT
======

[![Pipeline][pipeline]][link-pipeline]
[![Npm][npm-version]][link-npm]
[![Downloads][npm-downloads]][link-npm]
[![License][license]][link-license]
[![Node][node]][link-node]
[![Collaborators][collaborators]][link-npm]

### What is it?

This is **composable user acceptance and telemetry tracking** library. This library is using for collecting telemetry and user behaviour from page without big impact in application code. Telemetry can be composed of trigger event and have this logic on one place in your app. Module can be used separately, but it also can be used with special **react** implementation.

This is only signpost readme file. So if you want to learn what is [`@cuaatt/core`, go to module documentation][core]. If you want to know how to use [`@cuaatt/react` in react apps, go to module documentation][react].

### Install plain

In case that you don't want to use **react** do with [npm](https://npmjs.org):

```
npm install @cuaatt/core @cuaatt/params
```

### Install react

In case that you want to use **react** do with [npm](https://npmjs.org):

```
npm install @cuaatt/react
```

### Trackers

There is list of already implemented trackers that can be simple used with `@cuaatt/core` telemetry.

 1. [Google Analytics](https://gitlab.com/stanislavhacker/cuaatt/-/tree/master/module.tracker.gtag) - tracker for usage with Google Analytics service.


### How to run demo?

1. Download or clone this repo
2. Use `npm install` or `yarn install`
3. Use `npm run make-install`
4. Use `npm run preview`
5. Use generated url and open it in browser

### How to run tests?

After you download and install all (steps **1 - 3**)

1. Use `npm run test`

### How to run build?

After you download and install all (steps **1 - 3**)

1. Use `npm run build` for development build

**or**

1. Use `npm run build+minimize` for production build

### How to develop?

After you download and install all (steps **1 - 3**)

1. Use `npm run watch` to watch changes
2. Use `npm run preview`
3. Use generated url and open it in browser

### Donate me 😉

| QR | Paypal |
| ------ | ------ |
| ![](https://gitlab.com/uploads/-/system/personal_snippet/1929487/66399a49a06fa8eb9a0758b8673758c5/qr_sh.png) | [![](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DUT8W343NVGQQ&source=url) |


### License

MIT - [MIT License](https://spdx.org/licenses/MIT.html)

[logo]: https://gitlab.com/stanislavhacker/cuaatt/raw/master/logo.png

[pipeline]: https://gitlab.com/stanislavhacker/cuaatt/badges/master/pipeline.svg
[npm-version]: https://img.shields.io/npm/v/@cuaatt/core.svg
[npm-downloads]: https://img.shields.io/npm/dm/@cuaatt/core.svg
[license]: https://img.shields.io/npm/l/@cuaatt/core.svg
[node]: https://img.shields.io/node/v/@cuaatt/core.svg
[collaborators]: https://img.shields.io/npm/collaborators/@cuaatt/core.svg

[link-license]: https://gitlab.com/stanislavhacker/cuaatt/blob/master/LICENSE
[link-npm]: https://www.npmjs.com/package/@cuaatt/core
[link-pipeline]: https://gitlab.com/stanislavhacker/cuaatt/pipelines
[link-node]: https://nodejs.org/en/

[react]: https://gitlab.com/stanislavhacker/cuaatt/-/blob/master/module.react/README.md
[core]: https://gitlab.com/stanislavhacker/cuaatt/-/blob/master/module.core/README.md