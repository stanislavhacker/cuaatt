import {
	initTelemetry, checkCall, getCalls, checkDuration,
	DurationType, triggerFetch, triggerHttpRequest
} from "./helpers";
import {TelemetryOptionsNetworks} from "./helpers";

import {TelemetryResolve} from "../src";

describe("networks telemetry", () => {

	describe("networks settings", () => {
		let tel, clean, handler;

		beforeEach(() => {
			[tel, handler, clean] = initTelemetry(TelemetryOptionsNetworks);
		});
		afterEach(() => clean());

		it("check telemetry for fetch fail", async () => {
			await triggerFetch("http://www.this-is-totally-non-existing-url-boy.cz");

			const calls = await getCalls(handler);

			expect(calls.length).toBe(1);

			checkCall(calls[0], TelemetryResolve.Full, undefined, "request-fail", "request-fail", {
				url: 'http://www.this-is-totally-non-existing-url-boy.cz',
				method: 'GET',
				status: 0,
				error: jasmine.any(Error)
			});
			checkDuration(calls[0], DurationType.Instant);
		});

		it("check telemetry for xmlhttprequest fail", async () => {
			await triggerHttpRequest("GET", "http://www.this-is-totally-non-existing-url-boy.cz");

			const calls = await getCalls(handler);

			expect(calls.length).toBe(1);

			checkCall(calls[0], TelemetryResolve.Full, undefined, "request-fail", "request-fail", {
				url: 'http://www.this-is-totally-non-existing-url-boy.cz',
				method: 'GET',
				status: 0,
				error: jasmine.any(Error)
			});
			checkDuration(calls[0], DurationType.Instant);
		});

		it("check telemetry for fetch ok", async () => {
			await triggerFetch(window.location.href);

			const calls = await getCalls(handler);

			expect(calls.length).toBe(1);

			checkCall(calls[0], TelemetryResolve.Full, undefined, "request-ok", "request-ok", {
				url: window.location.href,
				method: 'GET',
				status: 200,
				error: undefined
			});
			checkDuration(calls[0], DurationType.Instant);
		});

		it("check telemetry for xmlhttprequest fail", async () => {
			await triggerHttpRequest("GET", window.location.href);

			const calls = await getCalls(handler);

			expect(calls.length).toBe(1);

			checkCall(calls[0], TelemetryResolve.Full, undefined, "request-ok", "request-ok", {
				url: window.location.href,
				method: 'GET',
				status: 200,
				error: undefined
			});
			checkDuration(calls[0], DurationType.Instant);
		});

	});

});