// language=Jade
export const templateOnlyZoneAndActions =
`div(data-tl-zone="main")
	a(href="https://www.seznam.cz", target="_blank", data-tl-action="link-click") Seznam.cz
	button(title="Button link", data-tl-action="link-click") Button link
	div.empty
`;

// language=Jade
export const templateOnlyContentZone =
`div(data-tl-zone="content") This is content
`;

// language=Jade
export const templateOnlyZoneAndActionsWithParameters =
`div(data-tl-zone="main")
	a(href="https://www.seznam.cz", target="_blank", data-tl-action="link-click", data-tl-params="[attr=href,target]") Seznam.cz
	button(id="main-button", title="Button link", data-tl-action="link-click", data-tl-params="[text][attr=title,id][val:b:valid='true'][val:n:count='22']") Button link
	div.empty
`;

// language=Jade
export const templateOnlyNavbarAndMenu =
`div(data-tl-zone="main")
	div(data-tl-zone="navigation")
		a(href="/#menu", data-tl-action="menu-click", data-tl-params="[attr=href][text]") Menu
		a(href="/#about", data-tl-action="link-click", data-tl-params="[attr=href][text]") About
	div.menu
`;

// language=Jade
export const templateOnlyMenuZone =
	`div(data-tl-zone="menu")
        a(href="/#link1", data-tl-action="link-click", data-tl-params="[attr=href][text]") Link 1
        a(href="/#link2", data-tl-action="link-click", data-tl-params="[attr=href][text]") Link 2
        a(href="/#link3", data-tl-action="link-click", data-tl-params="[attr=href][text]") Link 3
`;