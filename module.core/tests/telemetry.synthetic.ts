import {
	initTelemetry, renderStructure, checkCall, getCalls, checkDuration, renderTemplate,
	selectElement, clearElement, wait, DurationType, triggerEvent, triggerFetch
} from "./helpers";
import {TelemetryOptionsSynthetics, TelemetryOptionsSyntheticsNetwork} from "./helpers";
import {
	templateOnlyZoneAndActions,
	templateOnlyContentZone,
	templateOnlyNavbarAndMenu,
	templateOnlyMenuZone
} from "./html";

import {TelemetryResolve} from "../src";

describe("synthetics telemetry", () => {

	describe("synthetics settings", () => {
		let tel, clean, handler;

		beforeEach(() => {
			[tel, handler, clean] = initTelemetry(TelemetryOptionsSynthetics);
		});
		afterEach(() => clean());

		it("simple zone appear and disappear after", async () => {
			await renderStructure(templateOnlyZoneAndActions, async (el) => {
				const empty = await selectElement(el, "div.empty");
				await renderTemplate(templateOnlyContentZone, empty);
				await wait(100);
				await clearElement(el);

				const calls = await getCalls(handler);
				expect(calls.length).toBe(6);

				checkCall(calls[3], TelemetryResolve.Full, ["main", "content"], "zone-duration", "zone-duration");
				checkDuration(calls[3], DurationType.Long);
				checkCall(calls[5], TelemetryResolve.Full, ["main"], "zone-duration", "zone-duration");
				checkDuration(calls[5], DurationType.Long);
			});
		});

		it("simple zone appear and disappear after 2 times", async () => {
			await renderStructure(templateOnlyZoneAndActions, async (el) => {
				const empty = await selectElement(el, "div.empty");
				await renderTemplate(templateOnlyContentZone, empty);
				await wait(100);
				await clearElement(empty);
				await wait(100);
				await renderTemplate(templateOnlyContentZone, empty);
				await wait(100);
				await clearElement(empty);

				const calls = await getCalls(handler);
				expect(calls.length).toBe(7);

				checkCall(calls[3], TelemetryResolve.Full, ["main", "content"], "zone-duration", "zone-duration");
				checkDuration(calls[3], DurationType.Long);
				checkCall(calls[6], TelemetryResolve.Full, ["main", "content"], "zone-duration", "zone-duration");
				checkDuration(calls[6], DurationType.Long);
			});
		});

		it("telemetry to check menu open", async () => {
			await renderStructure(templateOnlyNavbarAndMenu, async (el) => {
				const menu = await selectElement(el, "div.menu");

				await triggerEvent(await selectElement(el, "a[href=\"/#menu\"]"), "click");
				await wait(20);
				await renderTemplate(templateOnlyMenuZone, menu);

				const calls = await getCalls(handler);
				expect(calls.length).toBe(5);

				checkCall(calls[4], TelemetryResolve.Full, ["main", "navigation"], "menu-opened", "menu-opened", {
					href: '/#menu',
					text: 'Menu'
				});
				checkDuration(calls[4], DurationType.Long);
			});
		});

	});

	describe("synthetic and network settings", () => {
		let tel, clean, handler;

		beforeEach(() => {
			[tel, handler, clean] = initTelemetry(TelemetryOptionsSyntheticsNetwork);
		});
		afterEach(() => clean());

		it("trigger success event duration", async () => {
			await renderStructure(templateOnlyMenuZone, async (el) => {
				await triggerEvent(await selectElement(el, "a[href=\"/#link1\"]"), "click");
				await wait(50);
				await triggerFetch(window.location.href);

				const calls = await getCalls(handler);
				expect(calls.length).toBe(4);

				checkCall(calls[3], TelemetryResolve.Full, ["menu"], "request-success-duration", "request-success-duration", {
					href: '/#link1',
					text: 'Link 1'
				});
				checkDuration(calls[3], DurationType.Long);
			});
		});

		it("trigger fail event duration", async () => {
			await renderStructure(templateOnlyMenuZone, async (el) => {
				await triggerEvent(await selectElement(el, "a[href=\"/#link1\"]"), "click");
				await wait(50);
				await triggerFetch("http://www.this-is-totally-non-existing-url-boy.cz");

				const calls = await getCalls(handler);
				expect(calls.length).toBe(4);

				checkCall(calls[3], TelemetryResolve.Full, ["menu"], "request-fail-duration", "request-fail-duration", {
					href: '/#link1',
					text: 'Link 1'
				});
				checkDuration(calls[3], DurationType.Long);
			});
		});

	});

});