import {
	initTelemetry, renderStructure, checkCall, getCalls, checkDuration, renderTemplate,
	selectElement, clearElement, DurationType
} from "./helpers";
import {TelemetryOptionsZones} from "./helpers";
import {templateOnlyZoneAndActions, templateOnlyContentZone} from "./html";

import {TelemetryResolve} from "../src";

describe("zones (mutations) telemetry", () => {

	describe("zone settings", () => {
		let tel, clean, handler;

		beforeEach(() => {
			[tel, handler, clean] = initTelemetry(TelemetryOptionsZones);
		});
		afterEach(() => clean());

		it("simple zone appear", async () => {
			await renderStructure(templateOnlyZoneAndActions, async (el) => {
				const empty = await selectElement(el, "div.empty");
				await renderTemplate(templateOnlyContentZone, empty);

				const calls = await getCalls(handler);
				expect(calls.length).toBe(2);
				checkCall(calls[0], TelemetryResolve.Partial, ["main"], null, "zone-appear");
				checkDuration(calls[0], DurationType.Instant);
				checkCall(calls[1], TelemetryResolve.Partial, ["main", "content"], null, "zone-appear");
				checkDuration(calls[1], DurationType.Instant);
			});
		});

		it("simple zone disappear", async () => {
			await renderStructure(templateOnlyZoneAndActions, async (el) => {
				const empty = await selectElement(el, "div.empty");
				await renderTemplate(templateOnlyContentZone, empty);
				await clearElement(empty);

				const calls = await getCalls(handler);
				expect(calls.length).toBe(3);
				checkCall(calls[0], TelemetryResolve.Partial, ["main"], null, "zone-appear");
				checkDuration(calls[0], DurationType.Instant);
				checkCall(calls[1], TelemetryResolve.Partial, ["main", "content"], null, "zone-appear");
				checkDuration(calls[1], DurationType.Instant);
				checkCall(calls[2], TelemetryResolve.Partial, ["main", "content"], null, "zone-disappear");
				checkDuration(calls[2], DurationType.Instant);
			});
		});

		it("simple zones disappear", async () => {
			await renderStructure(templateOnlyZoneAndActions, async (el) => {
				const empty = await selectElement(el, "div.empty");
				await renderTemplate(templateOnlyContentZone, empty);
				await clearElement(el);

				const calls = await getCalls(handler);
				expect(calls.length).toBe(4);
				checkCall(calls[0], TelemetryResolve.Partial, ["main"], null, "zone-appear");
				checkDuration(calls[0], DurationType.Instant);
				checkCall(calls[1], TelemetryResolve.Partial, ["main", "content"], null, "zone-appear");
				checkDuration(calls[1], DurationType.Instant);
				checkCall(calls[2], TelemetryResolve.Partial, ["main", "content"], null, "zone-disappear");
				checkDuration(calls[2], DurationType.Instant);
				checkCall(calls[3], TelemetryResolve.Partial, ["main"], null, "zone-disappear");
				checkDuration(calls[3], DurationType.Instant);
			});
		});

	});

});