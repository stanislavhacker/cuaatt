import {
	initTelemetry, renderStructure, triggerEvent, checkCall, getCalls, checkDuration, renderTemplate,
	selectElement, clearElement, DurationType
} from "./helpers";
import {TelemetryOptionsEmpty} from "./helpers";
import {templateOnlyZoneAndActions, templateOnlyContentZone, templateOnlyZoneAndActionsWithParameters} from "./html";

import {TelemetryResolve} from "../src";

describe("events telemetry", () => {

	describe("empty settings", () => {
		let tel, clean, handler;

		beforeEach(() => {
			[tel, handler, clean] = initTelemetry(TelemetryOptionsEmpty);
		});
		afterEach(() => clean());

		it("simple link click with zone", async () => {
			await renderStructure(templateOnlyZoneAndActions, async (el) => {
				await triggerEvent(await selectElement(el, "a"), "click");

				const calls = await getCalls(handler);
				expect(calls.length).toBe(1);
				checkCall(calls[0], TelemetryResolve.Global, ["main"], "link-click", "link-click");
				checkDuration(calls[0], DurationType.Instant);
			});
		});

		it("simple button click with zone", async () => {
			await renderStructure(templateOnlyZoneAndActions, async (el) => {
				await triggerEvent(await selectElement(el, "button"), "click");

				const calls = await getCalls(handler);
				expect(calls.length).toBe(1);
				checkCall(calls[0], TelemetryResolve.Global, ["main"], "link-click", "link-click");
				checkDuration(calls[0], DurationType.Instant);
			});
		});

		it("simple link with params click with zone", async () => {
			await renderStructure(templateOnlyZoneAndActionsWithParameters, async (el) => {
				await triggerEvent(await selectElement(el, "a"), "click");

				const calls = await getCalls(handler);
				expect(calls.length).toBe(1);
				checkCall(calls[0], TelemetryResolve.Global, ["main"], "link-click", "link-click", {
					href: 'https://www.seznam.cz',
					target: '_blank'
				});
				checkDuration(calls[0], DurationType.Instant);
			});
		});

		it("simple button with params click with zone", async () => {
			await renderStructure(templateOnlyZoneAndActionsWithParameters, async (el) => {
				await triggerEvent(await selectElement(el, "button"), "click");

				const calls = await getCalls(handler);
				expect(calls.length).toBe(1);
				checkCall(calls[0], TelemetryResolve.Global, ["main"], "link-click", "link-click", {
					title: 'Button link',
					id: 'main-button',
					text: 'Button link',
					valid: true,
					count: 22
				});
				checkDuration(calls[0], DurationType.Instant);
			});
		});

		it("simple zone appear is not handler with default settings", async () => {
			await renderStructure(templateOnlyZoneAndActions, async (el) => {
				const empty = await selectElement(el, "div.empty");
				await renderTemplate(templateOnlyContentZone, empty);

				const calls = await getCalls(handler);
				expect(calls.length).toBe(0);
			});
		});

		it("simple zone disappear is not handler with default settings", async () => {
			await renderStructure(templateOnlyZoneAndActions, async (el) => {
				const empty = await selectElement(el, "div.empty");
				await renderTemplate(templateOnlyContentZone, empty);
				await clearElement(empty);

				const calls = await getCalls(handler);
				expect(calls.length).toBe(0);
			});
		});

	});

});