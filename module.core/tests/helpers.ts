import * as pug from "browser-pug";
import {Params} from "@cuaatt/params";
import init, {TelemetryOptions, TelemetryResolve, TelemetryMessage} from "../src";

export const TelemetryEvents = ["mousedown", "click", "keydown", "keypress", "focus"] as const;

export type Zones = "main" | "navigation" | "content" | "quote" | "menu";
export type Actions = "link-click" | "button-click" | "menu-click";
export type Telemetry = Actions | "menu-opened" | "zone-appear" | "zone-disappear" | "zone-duration" | "request-fail" | "request-ok" | "request-success-duration" | "request-fail-duration";

export const TelemetryOptionsEmpty: OptionsShortcut = {
	events: TelemetryEvents
};
export const TelemetryOptionsZones: OptionsShortcut = {
	...TelemetryOptionsEmpty,
	direct: [
		{ type: "added", zone: "*", creates: "zone-appear" },
		{ type: "removed", zone: "*", creates: "zone-disappear" }
	]
};
export const TelemetryOptionsSynthetics: OptionsShortcut = {
	...TelemetryOptionsZones,
	synthetics: [
		{ start: { type: "added", zone: "content" }, end: { type: "removed", zone: "content" }, creates: "zone-duration" },
		{ start: { type: "added", zone: "main" }, end: { type: "removed", zone: "main" }, creates: "zone-duration" },
		{ start: { type: "click", action: "menu-click" }, end: { type: "added", zone: "menu" }, creates: "menu-opened" }
	]
};
export const TelemetryOptionsNetworks: OptionsShortcut = {
	...TelemetryOptionsZones,
	network: [
		{ method: "GET", url: "*", status: [/^(4[0-9]{2})/, 0], creates: "request-fail" },
		{ method: "GET", url: "*", status: /^(2[0-9]{2})/, creates: "request-ok" }
	]
};
export const TelemetryOptionsSyntheticsNetwork: OptionsShortcut = {
	...TelemetryOptionsSynthetics,
	...TelemetryOptionsNetworks,
	synthetics: [
		...(TelemetryOptionsSynthetics.synthetics || []),
		{ start: { type: "click", action: "link-click" }, end: { type: "network", action: "request-ok" }, creates: "request-success-duration" },
		{ start: { type: "click", action: "link-click" }, end: { type: "network", action: "request-fail" }, creates: "request-fail-duration" }
	]
};

type OptionsShortcut = TelemetryOptions<typeof TelemetryEvents[number], Zones, Actions, Telemetry>;
export function initTelemetry(opts: OptionsShortcut) {
	const handler = jasmine.createSpy("handler");
	const tel = init<Zones, Actions, Telemetry, typeof TelemetryEvents[number]>(handler, (defs) => ({ ...defs, ...opts }));
	return [tel, handler, () => tel.dispose()];
}

export async function renderStructure(template: string, handler: (el: Element) => Promise<void>): Promise<void> {
	const div = document.createElement("div");
	await renderTemplate(template, div);
	document.body.appendChild(div);
	await handler(div);
	document.body.removeChild(div);
}

export async function renderTemplate(template: string, el: Element | null): Promise<void> {
	const div = document.createElement("div");
	div.innerHTML = pug.render(template);

	if (el && div.firstChild) {
		el.appendChild(div.firstChild);
	}
}

export async function getCalls(handler: jasmine.Spy): Promise<Array<TelemetryMessage<typeof TelemetryEvents[number], Zones, Actions, Telemetry>>> {
	await wait(10);
	return handler.calls.all().map((call) => call.args[0]);
}

export async function selectElement(element: Element | null, selector: string): Promise<Element | null> {
	const el = element && element.querySelector(selector);
	if (!el) {
		throw new Error(`Element with selector "${selector}" not found in dom.`);
	}
	return el;
}

export async function clearElement(element: Element | null): Promise<void> {
	while (element && element.firstElementChild) {
		await removeElement(element.firstElementChild);
	}
}

export async function removeElement(element: Element | null): Promise<void> {
	element && element.parentElement && element.parentElement.removeChild(element);
}

export async function wait(timeout: number): Promise<void> {
	return new Promise((resolve) => setTimeout(resolve, timeout));
}

//TRIGGERS

export async function triggerEvent(element: Element | null, type: string): Promise<void> {
	let event;
	if (window.Event) {
		event = new Event(type, { bubbles: true, cancelable: true });
	} else {
		event = document.createEvent("HTMLEvents");
		event.initEvent(type, true, true);
		event.eventName = type;
	}
	if (element) {
		element.dispatchEvent(event);
	} else {
		throw new Error(`Element is not provided.`);
	}
}

export async function triggerFetch(url: RequestInfo, init?: RequestInit) {
	return await fetch(url, init).catch(() => void(0));
}

export async function triggerHttpRequest(method: string, url: string, body?: Document | XMLHttpRequestBodyInit | null) {
	return new Promise<void>((resolve, reject) => {
		const req = new XMLHttpRequest();
		req.open(method, url);
		req.send(body);
		req.addEventListener("readystatechange", () => {
			if (req.readyState === 4) {
				resolve();
			}
		});
		req.addEventListener("error", () => reject());
		req.addEventListener("timeout", () => reject());
	}).catch(() => void(0));
}

//CHECKS

export function checkCall(call: TelemetryMessage<typeof TelemetryEvents[number], Zones, Actions | Telemetry, Telemetry>,
						  resolve: TelemetryResolve, zones: Array<Zones> | undefined, action: Actions | Telemetry | null, telemetry: Telemetry, params: Params = {}) {
	expect(call).toEqual({
		time: {
			start: jasmine.anything(),
			end: jasmine.anything()
		},
		url: jasmine.anything(),
		resolve,
		...(zones ? { zones } : {}),
		action,
		telemetry,
		params
	});
}

export enum DurationType {
	Instant,
	Long
}
export function checkDuration(call: TelemetryMessage<typeof TelemetryEvents[number], Zones, Actions, Telemetry>,
							  dur: DurationType) {
	const duration = call.time.end - call.time.start;

	switch (dur) {
		case DurationType.Long:
			expect(duration).toBeGreaterThan(0);
			break;
		default:
			expect(duration).toBe(0);
			break;

	}
}