import {PreprocessTelemetries, DirectTelemetry} from "../data";
import {getZone, getAction} from "../utils";

export function preprocess<T, Z, A, M>(direct: Array<DirectTelemetry<T, Z, A, M>>): PreprocessTelemetries {
	const zones: Array<string> = [];
	const actions: Array<string> = [];

	direct.forEach((item) => {
		pushOneTime(zones, getZone<T, Z, A>(item));
		pushOneTime(actions, getAction<T, Z, A>(item));
	});

	return {
		actions,
		zones
	};
}

function pushOneTime<I>(items: Array<I>, item: I | null) {
	if (item && !items.includes(item)) {
		items.push(item);
	}
}