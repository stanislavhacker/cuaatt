import {TelemetryRecord, TelemetryOptions, TelemetryResolve, Synthetic, Network} from "../data";
import {findDirect} from "./find";
import {resolve} from "./resolve";

export function processDirect<T, Z, A, M>(telemetryRecord: TelemetryRecord<T, Z, A, M>, opts: Required<TelemetryOptions<T, Z, A, M>>): [M | A | undefined, TelemetryResolve]  {
	//synthetic telemetry is always full
	if (telemetryRecord.type as unknown as string === Synthetic) {
		return [telemetryRecord.action || undefined, TelemetryResolve.Full];
	}
	//network telemetry is always full
	if (telemetryRecord.type as unknown as string === Network) {
		return [telemetryRecord.action || undefined, TelemetryResolve.Full];
	}
	//direct telemetry
	const found = findDirect(opts.direct, telemetryRecord)[0];
	const telemetry = found.creates || telemetryRecord.action || undefined;
	return [telemetry, resolve(found)];
}