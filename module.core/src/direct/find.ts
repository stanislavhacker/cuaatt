import {DirectTelemetry, TelemetryRecord} from "../data";
import {getZoneAndAction} from "../telemetry/record";
import {matchItem} from "../utils";

export function findDirect<T, Z, A, M>(direct: Array<DirectTelemetry<T, Z, A, M>>, telemetryRecord: TelemetryRecord<T, Z, A, M>): Array<DirectTelemetry<T, Z, A, M>> {
    const { zone, action } = getZoneAndAction<T, Z, A, M>(telemetryRecord);
    const type = telemetryRecord.type;

    const found = direct.filter((drTel) => {
        const typeOk = matchItem(drTel.type, type);
        const zoneOk = zone ? matchItem(drTel.zone, zone) : true;
        const actionOk = action ? matchItem(drTel.action, action) : true;

        return typeOk && zoneOk && actionOk;
    });

    found.sort((a, b) => {
        let priorityA = getPriority(a.action);
        let priorityB = getPriority(b.action);

        if (priorityA !== priorityB) {
            return priorityB - priorityA;
        }

        priorityA = getPriority(a.zone);
        priorityB = getPriority(b.zone);

        return priorityB - priorityA;
    });
    return found;
}

function getPriority<T>(item: T | "*" | RegExp): number {
    switch (true) {
        case item === "*":
            return 1;
        case item instanceof RegExp:
            return 2;
        case typeof item === "string":
            return 3;
        default:
            return 4;
    }
}
