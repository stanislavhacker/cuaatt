import {DirectTelemetry, TelemetryResolve} from "../data";

export function resolve<T, Z, A, M>(telemetry: DirectTelemetry<T, Z, A, M>): TelemetryResolve {
	const type = telemetry.type === "*";
	const action = telemetry.action === "*";
	const zone = telemetry.zone === "*";

	if (type && action && zone) {
		return TelemetryResolve.Global;
	}
	if (type || action || zone) {
		return TelemetryResolve.Partial;
	}
	return TelemetryResolve.Full;
}