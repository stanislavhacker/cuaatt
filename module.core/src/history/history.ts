import {TelemetryRecord, TelemetryCache} from "../data";
import {usedIn} from "../synthetics/used";

export function pushHistory<T, Z, A, M>(telemetryInfo: TelemetryRecord<T, Z, A, M>, cache?: TelemetryCache) {
	const id = usedIn(telemetryInfo, cache);

	if (cache && id) {
		cache.history[id] = telemetryInfo;
	}
}