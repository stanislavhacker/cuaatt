import {TelemetryCache, TelemetryOptions, TelemetryMessage} from "../data";
import {RequestType, RequestData} from "../network/data";
import {processTelemetryNetwork} from "../procesors/network";
import {processTelemetry} from "../telemetry/process";

export function network<T, Z, A, M>(type: RequestType, data: RequestData[typeof type], handler: (telemetryInfo: TelemetryMessage<T, Z, A, A | M>) => void,
									  opts: Required<TelemetryOptions<T, Z, A, M>>, cache: TelemetryCache): void {
	const telemetryRecord = processTelemetryNetwork<T, Z, A, M>(opts, type, data);

	if (telemetryRecord) {
		processTelemetry(telemetryRecord, handler, opts, cache);
	}
}