import {TelemetryCache, TelemetryOptions, TelemetryMessage} from "../data";
import { processTelemetryEvent } from "../procesors/event";
import { processTelemetry } from "../telemetry/process";

export function events<T, Z, A, M>(ev: Event, handler: (telemetryInfo: TelemetryMessage<T, Z, A, A | M>) => void,
                                   opts: Required<TelemetryOptions<T, Z, A, M>>, cache: TelemetryCache): void {

    const telemetryRecord = processTelemetryEvent<T, Z, A, M>(opts, ev.type as unknown as T, ev.target as Element);

    if (telemetryRecord) {
        processTelemetry<T, Z, A, M>(telemetryRecord, handler, opts, cache);
    }
}
