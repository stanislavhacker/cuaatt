import {TelemetryCache, TelemetryOptions, TelemetryMessage, TelemetryRecord, SpecialEvents, Added, Removed} from "../data";
import {findNodesWithInfo, getDomInfo} from "../dom/elements";
import {processTelemetryMutation} from "../procesors/mutation";
import {processTelemetry} from "../telemetry/process";

export function mutations<T, Z, A, M>(mutations: MutationRecord[], handler: (telemetryInfo: TelemetryMessage<T, Z, A, A | M>) => void,
								   opts: Required<TelemetryOptions<T, Z, A, M>>, cache: TelemetryCache): void {
	const addedNodes: Array<[Node, Element]> = [];
	const removedNodes: Array<[Node, Element]> = [];
	const ids: Array<string> = [];

	mutations.forEach((mutation) => {
		mutation.addedNodes.forEach((node) => processNodes(opts, mutation.target, node, addedNodes, true));
		mutation.removedNodes.forEach((node) => processNodes(opts, mutation.target, node, removedNodes, false));
	});

	const addedItems: Array<TelemetryRecord<T, Z, A, M>> = [];
	addedNodes.forEach((el) => processNode(opts, el, Added, addedItems, ids));
	addedItems.sort((a, b) =>
		(a.zones || []).length - (b.zones || []).length);
	const removedItems: Array<TelemetryRecord<T, Z, A, M>> = [];
	removedNodes.forEach((el) => processNode(opts, el, Removed, removedItems, ids));
	removedItems.sort((a, b) =>
		(b.zones || []).length - (a.zones || []).length);

	const filtered = processGroups<T, Z, A, M>([...addedItems, ...removedItems]);
	filtered.forEach((telemetryRecord) => {
		processTelemetry(telemetryRecord, handler, opts, cache);
	});
}

function processNodes<T, Z, A, M>(opts: Required<TelemetryOptions<T, Z, A, M>>, target: Node, node: Node, nodes: Array<[Node, Element]>, connectedOnly: boolean) {
    const el = node as Element;

    if (!el.querySelectorAll) {
        return;
    }
    if (connectedOnly && !el.isConnected) {
        return;
    }

	findNodesWithInfo<T, Z, A, M>(opts, node).forEach((inner) => {
        if (!nodes.some(([, item]) => item === inner)) {
            nodes.push([target, inner]);
        }
    });
}

function processNode<T, Z, A, M>(opts: Required<TelemetryOptions<T, Z, A, M>>, [target, el]: [Node, Element], type: typeof SpecialEvents[number], items: Array<TelemetryRecord<T, Z, A, M>>, ids: Array<string>) {
    let zones = processZones(opts, [target, el], type);

    if (zones.length) {
        const info = processTelemetryMutation<T, Z, A, M>(opts, type as unknown as T, el, zones);
        if (info && !ids.includes(info.id)) {
            items.push(info);
            ids.push(info.id);
        }
    }
}

function processGroups<T, Z, A, M>(items: Array<TelemetryRecord<T, Z, A, M>>) {
	const grouped = items.reduce((grouped, item) => {
		const id = getGroupId(item);
		grouped[id] = [...(grouped[id] || []), item];
		return grouped;
	}, {} as { [key: string]: Array<TelemetryRecord<T, Z, A, M>> });

	return Object.keys(grouped).reduce((prev, key) => {
		const items = grouped[key];

		if (items.length > 1) {
			const added = items.filter((item) => item.type as unknown as typeof SpecialEvents[number] === Added);
			const removed = items.filter((item) => item.type as unknown as typeof SpecialEvents[number] === Removed);

			if (added > removed) {
				prev.push(added.pop()!);
			}
			if (added < removed) {
				prev.push(removed.pop()!);
			}

		} else {
			prev.push(items[0]);
		}

		return prev;
	}, [] as Array<TelemetryRecord<T, Z, A, M>>);
}

function processZones<T, Z, A, M>(opts: Required<TelemetryOptions<T, Z, A, M>>, [target, el]: [Node, Element], type: typeof SpecialEvents[number]) {
	switch (type) {
		case Removed:
			return [...getDomInfo(opts, target).zones, ...getDomInfo(opts, el).zones];
		default:
			return getDomInfo(opts, el).zones;
	}
}

function getGroupId<T, Z, A, M>(item: TelemetryRecord<T, Z, A, M>) {
	return `${(item.zones || []).join(",")}/${item.action || "??"}/${String(item.params)}`;
}