import {Params} from "@cuaatt/params";

import {NetworkInfo} from "./network/info";

export const Synthetic = "synthetic";
export const Added = "added";
export const Removed = "removed";
export const Network = "network";

export const DefaultEvents = ["mouseup", "keyup", "click"] as const;
export const SpecialEvents = [Synthetic, Added,  Removed, Network] as const;
export const ZoneAttributes = ["data-tl-zone"] as const;
export const ActionAttributes = ["data-tl-action"] as const;
export const ParamsAttributes = ["data-tl-params"] as const;

export type NetworkMethods = "GET" | "HEAD" | "POST" | "PUT" | "DELETE" | "CONNECT" | "OPTIONS" | "TRACE" | "PATCH";
export type NetworkStatusCodes = 0 | 100 | 101 | 102 | 103 |
	200 | 201 | 202 | 203 | 204 | 205 | 206 | 207 | 208 | 226 |
	300 | 301 | 302 | 303 | 304 | 305 | 306 | 307 | 308 |
	400 | 401 | 402 | 403 | 404 | 405 | 406 | 407 | 408 | 409 | 410 | 411 | 412 | 413 | 414 | 415 | 416 | 417 | 418 | 421 | 422 | 423 | 424 | 425 | 426 | 428 | 429 | 431 | 451 |
	500 | 501 | 502 | 5036 | 504 | 505 | 506 | 507 | 508 | 510 | 511;

export interface Telemetry<T, Z, A, M> {
	dispose(): void;
	options(): Required<TelemetryOptions<T, Z, A, M | A>>;
}

export interface TelemetryHandler<T, Z, A, M> {
	(telemetryInfo: TelemetryMessage<T, Z, A, M>): void;
	dispose?: () => void;
}

export enum TelemetryResolve {
	Global = 0,
	Partial = 1,
	Full = 2
}

export type TelemetryRecord<T, Z, A, M> = {
	time: {
		start: number;
		end: number;
	}
	resolve: TelemetryResolve;
	url: string;
	zones?: Array<Z>;
	action?: A | null;
	telemetry?: M;
	params?: Params | NetworkInfo;
	//removed
	id: string;
	type: T;
};

export type TelemetryMessage<T, Z, A, M> = Readonly<Omit<TelemetryRecord<T | typeof SpecialEvents[number], Z, A, A | M>, "id" | "type">>;

export type TelemetryPoint<T, Z, A> = {
	type: T;
	action?: A;
	zone?: Z;
}

export type SyntheticTelemetry<T, Z, A, M> = {
	start: TelemetryPoint<T, Z, A | M>;
	end: TelemetryPoint<T, Z, A | M>;
	creates: M;
}
export type DirectTelemetry<T, Z, A, M> = {
	type: T | RegExp | "*";
	action?: A | RegExp | "*";
	zone?: Z | RegExp | "*";
	creates?: M;
}
export type NetworkTelemetry<M> = {
	method: NetworkMethods | Array<NetworkMethods> | "*";
	status: NetworkStatusCodes | Array<NetworkStatusCodes | RegExp> | RegExp | "*";
	url: string | RegExp | "*";
	creates: M;
}

export type TelemetryOptions<T, Z, A, M> = {
	events: Readonly<Array<T | typeof SpecialEvents[number]>>;
	zoneAttributes?: Readonly<Array<string>>;
	actionAttributes?: Readonly<Array<string>>;
	paramsAttributes?: Readonly<Array<string>>;
	direct?: Array<DirectTelemetry<T | typeof SpecialEvents[number], Z, A, A | M>>;
	network?: Array<NetworkTelemetry<A | M>>;
	synthetics?: Array<SyntheticTelemetry<T | typeof SpecialEvents[number], Z, A, A | M>>;
}

export type PreprocessTelemetries  = {
	actions: Array<string>;
	zones: Array<string>;
}

export type TelemetryCache = {
	synthetic: PreprocessTelemetries;
	direct: PreprocessTelemetries;
	network: Array<NetworkTelemetry<any>>;
	history: {
		[key: string]: TelemetryRecord<any, any, any, any>;
	}
}

export type TelemetryElementInfo = {
	zone?: string;
	action?: string;
	params?: Params;
}

export type TelemetryDomInfo<Z, A> = {
	zones: Array<Z>;
	action?: A;
	params?: Params;
}