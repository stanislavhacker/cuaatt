import {TelemetryRecord, TelemetryOptions, TelemetryMessage, TelemetryCache, SyntheticTelemetry} from "../data";
import {findSynthetics} from "./find";
import {processTelemetrySynthetic} from "../procesors/synthetic";
import {getTelemetryIdFromSynthetic} from "../utils";
import {processTelemetry} from "../telemetry/process";

const store = new WeakMap<TelemetryRecord<any, any, any, any>, Array<string>>();

export function processSynthetics<T, Z, A, M>(telemetryRecord: TelemetryRecord<T, Z, A, M>, handler: (telemetryInfo: TelemetryMessage<T, Z, A, A | M>) => void,
											  opts: Required<TelemetryOptions<T, Z, A, M>>, cache: TelemetryCache)  {
	const found = findSynthetics(opts.synthetics, telemetryRecord, "end");

	found.forEach((item) => {
		const id = getTelemetryIdFromSynthetic(item);
		const saved = id && cache.history[id];

		if (id && saved) {
			const data = processTelemetrySynthetic<T, Z, A, M>(item as SyntheticTelemetry<T, Z, A, M>, saved as TelemetryRecord<T, Z, A, M>);
			const triggered = store.get(saved) || store.set(saved, []).get(saved)!;

			if (triggered.indexOf(data.id) === -1) {
				triggered.push(data.id);
				processTelemetry<T, Z, A, M>(data, handler, opts, cache);
			}
		}
	});
}