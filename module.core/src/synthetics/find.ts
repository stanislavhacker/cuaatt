import {SyntheticTelemetry, TelemetryRecord} from "../data";
import {getZoneAndAction} from "../telemetry/record";
import {matchItem} from "../utils";


export function findSynthetics<T, Z, A, M>(synthetics: Array<SyntheticTelemetry<T, Z, A, M>>, telemetryRecord: TelemetryRecord<T, Z, A, M>, what: keyof Pick<SyntheticTelemetry<T, Z, A, M>, "start" | "end">): Array<SyntheticTelemetry<T, Z, A, M>> {
	const { zone, action } = getZoneAndAction(telemetryRecord);
	const type = telemetryRecord.type;

	return synthetics.filter((synth) => {
		const typeOk = matchItem(synth[what].type, type);
		const zoneOk = zone ? matchItem(synth[what].zone, zone) : true;
		const actionOk = action ? matchItem(synth[what].action, action) : true;

		return typeOk && zoneOk && actionOk;
	});
}