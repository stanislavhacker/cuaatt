import {TelemetryRecord, TelemetryCache} from "../data";
import {getTelemetryId} from "../utils";

export function usedIn<T, Z, A, M>(telemetryInfo: TelemetryRecord<T, Z, A, M>, cache?: TelemetryCache): string | null {
	const id = getTelemetryId(telemetryInfo);

	if (cache && id) {
		const { zones, actions } = cache.synthetic;
		if (actions.includes(id) || zones.includes(id)) {
			return id;
		}
	}
	return null;
}