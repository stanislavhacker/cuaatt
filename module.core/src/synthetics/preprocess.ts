import {SyntheticTelemetry, PreprocessTelemetries} from "../data";
import {getZone, getAction} from "../utils";

export function preprocess<T, Z, A, M>(synthetics: Array<SyntheticTelemetry<T, Z, A, M>>): PreprocessTelemetries {
	const zones: Array<string> = [];
	const actions: Array<string> = [];

	synthetics.forEach((item) => {
		pushOneTime(zones, getZone<T, Z, A | M>(item.start));
		pushOneTime(zones, getZone<T, Z, A | M>(item.end));
		pushOneTime(actions, getAction<T, Z, A | M>(item.start));
		pushOneTime(actions, getAction<T, Z, A | M>(item.end));
	});

	return {
		actions,
		zones
	};
}

function pushOneTime<I>(items: Array<I>, item: I | null) {
	if (item && !items.includes(item)) {
		items.push(item);
	}
}