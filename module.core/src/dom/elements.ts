import {parse, Params} from "@cuaatt/params";

import {TelemetryOptions, TelemetryElementInfo, TelemetryDomInfo} from "../data";

export function getDomInfo<T, Z, A, M>(opts: Required<TelemetryOptions<T, Z, A, M>>, target: Node | Element | null): TelemetryDomInfo<Z, A> {
	const allZones: Array<Z> = [];
	let firstAction: A | undefined = undefined;
	let firstParams: Params | undefined;
	let el = target;

	while (el) {
		const {zone, action, params} = processElementData<T, Z, A, M>(opts, el);
		zone && allZones.unshift(zone as unknown as Z);
		firstAction = firstAction || action as unknown as A;
		firstParams = firstParams || params;
		el = el.parentElement;
	}

	return {
		zones: allZones,
		action: firstAction,
		params: firstParams
	}
}

export function findNodesWithInfo<T, Z, A, M>(opts: Required<TelemetryOptions<T, Z, A, M>>, where: Node | Element | null): Array<Element> {
	const attrs = opts.zoneAttributes;
	const wh = where as Element;

	if (!wh || !wh.querySelectorAll) {
		return [];
	}

	return attrs.reduce((prev, attr) => {
		const sel = `*[${attr}]`;
		return [
			...prev,
			...(wh.matches(sel) ? [wh] : []),
			...Array.prototype.slice.call(wh.querySelectorAll(sel))
		];
	}, []);
}

function processElementData<T, Z, A, M>(opts: Required<TelemetryOptions<T, Z, A, M>>, node: Element | Node): TelemetryElementInfo {
	const { zoneAttributes, actionAttributes, paramsAttributes } = opts;
	const el = node as Element;

	const zone = getAttrInfo(el, zoneAttributes);
	const action = getAttrInfo(el, actionAttributes);
	const params = getAttrInfo(el, paramsAttributes);

	return { zone: zone || undefined, action: action || undefined, params: parse(el, params) };
}

function getAttrInfo(el: Element, attributes: Readonly<Array<string>>): string {
	if (el.getAttribute) {
		const attrs = attributes.map((attribute) => el.getAttribute(attribute) as string).filter(Boolean);

		if (attrs.length) {
			return attrs[0];
		}
	}
	return "";
}