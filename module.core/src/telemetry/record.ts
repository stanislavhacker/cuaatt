import {Params} from "@cuaatt/params";

import {TelemetryRecord, SyntheticTelemetry, TelemetryResolve, Synthetic, Network} from "../data";
import {NetworkInfo} from "../network/info";

export function createDirectRecord<T, Z, A, M>(type: T, zones: Array<Z>, action: A | null, params: Params | undefined): TelemetryRecord<T, Z, A, M> {
	const date = new Date().getTime();

	return {
		id: getDirectId<T, Z, A>(date, type, zones, action),
		type,
		time: {
			start: date,
			end: date
		},
		url: getUrl(),
		resolve: TelemetryResolve.Full,
		action: action || null,
		zones: zones || [],
		params,
	};
}

export function createSyntheticRecord<T, Z, A, M>(item: SyntheticTelemetry<T, Z, A, M>, record: TelemetryRecord<T, Z, A, M>): TelemetryRecord<T, Z, A, M> {
	const date = new Date().getTime();

	return {
		id: getSyntheticId<T, Z, A, M>(item, record),
		type: Synthetic as unknown as T,
		time: {
			start: record.time.end,
			end: date
		},
		resolve: TelemetryResolve.Full,
		action: (item.creates || record.action || null) as unknown as A,
		url: record.url,
		zones: record.zones || [],
		params: record.params,
	};
}

export function createNetworkRecord<T, Z, A, M>(action: A, params: NetworkInfo): TelemetryRecord<T, Z, A, M> {
	const date = new Date().getTime();

	return {
		id: getNetworkId<T, Z, A>(date, action, params.url),
		type: Network as unknown as T,
		time: {
			start: date,
			end: date
		},
		url: getUrl(),
		resolve: TelemetryResolve.Full,
		action: action,
		params,
	};
}

export function getDirectId<T, Z, A>(start: number, type: T, zones: Array<Z>, action: A | null): string {
    return `${start}-${type}-${zones.join(",") || "??"}-${action || "??"}`;
}

export function getSyntheticId<T, Z, A, M>(item: SyntheticTelemetry<T, Z, A, M>, data: TelemetryRecord<T, Z, A, M>): string {
    return `${data.id}-${Synthetic}-${item.creates || "??"}`;
}

export function getNetworkId<T, Z, A>(start: number, action: A | null, url: string): string {
	return `${start}-${Network}-${action || "??"}-${url}`;
}

export function getZoneAndAction<T, Z, A, M>(record: TelemetryRecord<T, Z, A, M>): { action: A | null , zone: Z | null } {
    const action = record.action || null;
    const zone = record.zones && record.zones[record.zones.length - 1] || null;

    return { action, zone };
}

function getUrl() {
	if (window && window.location) {
		return window.location.toString();
	}
	return "";
}