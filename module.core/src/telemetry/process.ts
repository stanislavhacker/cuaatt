import {TelemetryCache, TelemetryMessage, TelemetryRecord, TelemetryOptions, TelemetryResolve} from "../data";
import {processDirect} from "../direct/process";
import {processSynthetics} from "../synthetics/process";
import {pushHistory} from "../history/history";

export function processTelemetry<T, Z, A, M>(telemetryRecord: TelemetryRecord<T, Z, A, M>, handler: (telemetryInfo: TelemetryMessage<T, Z, A, A | M>) => void,
											 opts: Required<TelemetryOptions<T, Z, A, M>>, cache: TelemetryCache): void {
	const [telemetry, resolve] = processDirect(telemetryRecord, opts);

	telemetry && handler(getTelemetryMessage<T, Z, A, M>(telemetryRecord, telemetry, resolve));
	pushHistory(telemetryRecord, cache);
	processSynthetics<T, Z, A, M>(telemetryRecord, handler, opts, cache);
}

function getTelemetryMessage<T, Z, A, M>(telemetryRecord: TelemetryRecord<T, Z, A, M>, telemetry: A | M | undefined, resolve: TelemetryResolve): TelemetryMessage<T, Z, A, M> {
	const record = {...telemetryRecord, telemetry} as Partial<TelemetryRecord<T, Z, A, M>>;
	delete record["id"];
	delete record["type"];
	record.resolve = resolve;
	return record as TelemetryMessage<T, Z, A, M>;
}