import {createSyntheticRecord} from "../telemetry/record";
import {SyntheticTelemetry, TelemetryRecord} from "../data";


export function processTelemetrySynthetic<T, Z, A, M>(item: SyntheticTelemetry<T, Z, A, M>, prev: TelemetryRecord<T, Z, A, M>): TelemetryRecord<T, Z, A, M> {
	return createSyntheticRecord<T, Z, A, M>(item, prev);
}