import {TelemetryRecord, TelemetryOptions} from "../data";
import {RequestType, RequestData} from "../network/data";
import {createNetworkRecord} from "../telemetry/record";
import {findNetwork} from "../network/find";
import {getNetworkInfo} from "../network/info";

export function processTelemetryNetwork<T, Z, A, M>(opts: Required<TelemetryOptions<T, Z, A, M>>, type: RequestType, data: RequestData[typeof type]): TelemetryRecord<T, Z, A, M> | null {
	const found = findNetwork(opts.network, type, data)[0];

	if (found) {
		return createNetworkRecord<T, Z, A, M>(found.creates as unknown as A, getNetworkInfo(type,  data));
	}
	return null;
}