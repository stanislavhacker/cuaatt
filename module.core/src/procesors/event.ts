import {TelemetryRecord, TelemetryOptions} from "../data";
import {getDomInfo} from "../dom/elements";
import {createDirectRecord} from "../telemetry/record";

export function processTelemetryEvent<T, Z, A, M>(opts: Required<TelemetryOptions<T, Z, A, M>>, type: T, target: Node | Element | null): TelemetryRecord<T, Z, A, M> | null {
	const {zones, action, params} = getDomInfo<T, Z, A, M>(opts, target);

	if (zones.length > 0 || action) {
		return createDirectRecord<T, Z, A, M>(type, zones, action || null, params);
	}
	return null;
}