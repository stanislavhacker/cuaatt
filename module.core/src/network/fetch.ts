import { NetworkHandler, RequestType } from "./data";

const RealFetch = window.fetch;

export function FetchInterceptor(handler: NetworkHandler): () => void {
	//no fetch implemented
	if (!window.fetch) {
		return () => void(0);
	}

	window.fetch = function () {
		const [request, init] = [arguments[0], arguments[1]];
		return fetchIntercept(this, request, init, handler);
	}

	return () => {
		window.fetch = RealFetch;
	}
}

function fetchIntercept(self: any, request: RequestInfo, init: RequestInit | undefined, handler: NetworkHandler): Promise<Response> {
	return new Promise(function (resolve, reject) {
		RealFetch.apply(self, [request, init])
			.then((response: Response) => {
				handler(RequestType.Fetch, {
					request: cloneRequestInfo(request),
					response: response.clone(),
					init,
				});
				resolve(response);
			})
			.catch((error: Error) => {
				handler(RequestType.Fetch, {
					request: cloneRequestInfo(request),
					init,
					error
				});
				reject(error);
			})
	});
}

function cloneRequestInfo(request: RequestInfo): RequestInfo {
	if (typeof request === "string") {
		return request;
	}
	return request.clone();
}