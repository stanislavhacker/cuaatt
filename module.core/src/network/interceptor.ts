import {NetworkHandler} from "./data";
import {FetchInterceptor} from "./fetch";
import { XMLHttpRequestInterceptor } from "./request";

export function interceptor(handler: NetworkHandler): () => void {

	const xmlHttpRequestClean = XMLHttpRequestInterceptor(handler);
	const fetchClean = FetchInterceptor(handler);

	return () => {
		xmlHttpRequestClean();
		fetchClean();
	}
}