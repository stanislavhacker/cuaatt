import {NetworkMethods, NetworkStatusCodes} from "../data";
import {RequestType, RequestData} from "./data";

export type NetworkInfo = {
	method: NetworkMethods;
	status: NetworkStatusCodes;
	url: string;
	error?: Error;
}
export function getNetworkInfo(type: RequestType, data: RequestData[typeof type]): NetworkInfo {
	switch (type) {
		case RequestType.Ajax:
			const reqData = data as RequestData[RequestType.Ajax];
			return {
				url: reqData.request.url,
				method: reqData.request.method.toUpperCase() as NetworkMethods,
				status: reqData.response ? reqData.response.status as NetworkStatusCodes : 0,
				error: reqData.response && reqData.response.status > 0 ? undefined : new Error("Can not get response.")
			};
		case RequestType.Fetch:
			const fetchData = data as RequestData[RequestType.Fetch];
			return {
				url: retrieveFromRequestInfo(fetchData.request, fetchData.request as string, (req) => req.url),
				method: retrieveFromRequestInfo<NetworkMethods>(fetchData.request, "GET", (req) => req.method.toUpperCase() as NetworkMethods),
				status: fetchData.response ? fetchData.response.status as NetworkStatusCodes : 0,
				error: fetchData.error || undefined
			};
		default:
			throw new Error(`Unknown request type.`);
	}
}

function retrieveFromRequestInfo<T>(info: RequestInfo, defaultValue: T, getter: (req: Request) => T): T {
	if (typeof info === "string") {
		return defaultValue;
	}
	return getter(info);
}