import {NetworkHandler, RequestType, RequestData} from "./data";

const RealXHRSend = XMLHttpRequest.prototype.send;
const RealXHROpen = XMLHttpRequest.prototype.open;

const map = new WeakMap<XMLHttpRequest, RequestData[RequestType.Ajax]>();

export function XMLHttpRequestInterceptor(handler: NetworkHandler): () => void {
	//no xml http request
	if (typeof XMLHttpRequest === "undefined") {
		return () => void(0);
	}

	XMLHttpRequest.prototype.open = function () {
		const self = this;
		const [method, url, async, user, password] = Array.prototype.slice.call(arguments);

		map.set(self, {
			request: {url, method, async, user, password }
		});
		RealXHROpen.apply(self, arguments);
	};

	XMLHttpRequest.prototype.send = function () {
		const self = this;
		const [body] = Array.prototype.slice.call(arguments);

		const data = map.get(self) || { request: { method: "GET", url: "" } };
		data.request.body = body;

		xmlRequestDone(self, () => {
			data.request.url = self.responseURL || data.request.url;
			data.response = {
				type: self.responseType,
				content: self.response,
				status: self.status,
				statusText: self.statusText
			}
			handler(RequestType.Ajax, data);
		});
		RealXHRSend.apply(self, arguments);
	};

	return () => {
		XMLHttpRequest.prototype.send = RealXHRSend;
		XMLHttpRequest.prototype.open = RealXHROpen;
	}
}

function xmlRequestDone(xhr: XMLHttpRequest, handler) {
	if (xhr.addEventListener) {
		xhr.addEventListener("readystatechange", function () {
			if(xhr.readyState === 4) {
				handler();
			}
		}, false);
	} else {
		const realOnReadyStateChange = this.onreadystatechange;
		xhr.onreadystatechange = function () {
			if(xhr.readyState === 4) {
				handler();
			}
			realOnReadyStateChange();
		};
	}
}