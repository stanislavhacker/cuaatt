
export enum RequestType {
	Fetch = "fetch",
	Ajax = "ajax"
}

type XMLHttpRequest = {
	method: string;
	url: string;
	async?: boolean;
	user?: string | null;
	password?: string | null;
	body?: Document | BodyInit | null;
};
type XMLHttpResponse = {
	type: string;
	content: any;
	status: number;
	statusText: string;
};

export type RequestData = {
	[RequestType.Fetch]: {
		request: RequestInfo;
		init?: RequestInit;
		response?: Response;
		error?: Error;
	},
	[RequestType.Ajax]: {
		request: XMLHttpRequest;
		response?: XMLHttpResponse;
	}
}

export type NetworkHandler = (type: RequestType, data: RequestData[typeof type]) => void;