import {NetworkTelemetry} from "../data";
import {RequestType, RequestData} from "./data";
import {matchItem} from "../utils";
import {getNetworkInfo} from "./info";

export function findNetwork<T, Z, A, M>(network: Array<NetworkTelemetry<M>>, type: RequestType, data: RequestData[typeof type]): Array<NetworkTelemetry<M>> {
	const { status, url, method } = getNetworkInfo(type, data);

	return network.filter((ntTel) => {
		const urlOk = matchItem(ntTel.url, url);
		const methodOk = matchItem(ntTel.method, method);
		const statusOk = matchItem(ntTel.status, status);

		return urlOk && methodOk && statusOk;
	});
}

