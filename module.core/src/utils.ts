import {TelemetryPoint, DirectTelemetry, TelemetryRecord, SyntheticTelemetry} from "./data";
import {getZoneAndAction} from "./telemetry/record";

export function getAction<T, Z, A>(item: TelemetryPoint<T, Z, A> | DirectTelemetry<T, Z, A, any>): string | null {
	return item.action ? getId(item.type, item.action) : null;
}
export function getZone<T, Z, A>(item: TelemetryPoint<T, Z, A> | DirectTelemetry<T, Z, A, any>): string | null {
	return item.zone ? getId(item.type, item.zone) : null;
}
export function getId<T, Z, A>(type: T, what: Z | A) {
	return `${what}[${type}]`;
}

export function matchItem<T>(item: T | "*" | RegExp | Array<T>, match: T): boolean {
	switch (true) {
		case item === "*":
			return true;
		case item instanceof RegExp:
			return (item as RegExp).test(match as unknown as string);
		case Array.isArray(item):
			return (item as Array<T>).some((itm) => matchItem(itm, match));
		default:
			return item === match;
	}
}

export function getTelemetryId<T, Z, A, M>(telemetryInfo: TelemetryRecord<T, Z, A, M>): string | null {
    const { zone, action } = getZoneAndAction(telemetryInfo);

    if (action) {
        return getId(telemetryInfo.type, action);
    }

    if (zone) {
        return getId(telemetryInfo.type, zone);
    }

    return null;
}
export function getTelemetryIdFromSynthetic<T, Z, A, M>(synthetic: SyntheticTelemetry<T, Z, A, M>): string | null {
    if (synthetic.start.action) {
        return getId(synthetic.start.type, synthetic.start.action);
    }

    if (synthetic.start.zone) {
        return getId(synthetic.start.type, synthetic.start.zone);
    }

    return null;
}
