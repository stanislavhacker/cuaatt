import {Telemetry, TelemetryOptions, ZoneAttributes, ActionAttributes, ParamsAttributes, DefaultEvents, DirectTelemetry, TelemetryCache, SyntheticTelemetry, TelemetryHandler} from "./data";
import * as synth from "./synthetics/preprocess";
import * as drct from "./direct/preprocess";
import * as events from "./translators/events";
import * as mutations from "./translators/mutations";
import * as network from "./translators/network";
import * as interceptor from "./network/interceptor";

export function init<Z, A, M, T = typeof DefaultEvents[number]>(
	handler: TelemetryHandler<T, Z, A, M>,
	options: (defaults: TelemetryOptions<T, Z, A, A | M>) => TelemetryOptions<T, Z, A, M> = (def) => def as TelemetryOptions<T, Z, A, M>
): Telemetry<T, Z, A, M> {
	const opts = enhancedOption(options(buildOptions<T, Z, A, A | M>()));
	const cache = cacheOptions(opts);

	const handlers = opts.events.map((event) => {
		const handle = (e: Event) => events.events(e, handler, opts, cache);
		window.addEventListener(event as any, handle, true);
		return handle;
	});
	const dispose = interceptor.interceptor((type, data) => network.network(type, data, handler, opts, cache));

	const observer = new MutationObserver((records) => mutations.mutations(records, handler, opts, cache));
	observer.observe(document.body, { subtree: true, childList: true });

	return {
		options: () => opts,
		dispose: () => {
			handlers.forEach((handler, i) => {
				window.removeEventListener(opts.events[i] as any, handler, true);
			});
			observer.disconnect();
			dispose();
			handler && handler.dispose && handler.dispose();
		}
	}
}

function buildOptions<T, Z, A, M>(): Required<TelemetryOptions<T, Z, A, M>> {
	return {
		events: DefaultEvents as unknown as Array<T>,
		actionAttributes: ActionAttributes,
		zoneAttributes: ZoneAttributes,
		paramsAttributes: ParamsAttributes,
		direct: [],
		network: [],
		synthetics: []
	}
}
function enhancedOption<T, Z, A, M>(opts: TelemetryOptions<T, Z, A, M>): Required<TelemetryOptions<T, Z, A, M>> {
	return {
		...buildOptions(),
		...opts
	}
}

function cacheOptions<T, Z, A, M>(opts: Required<TelemetryOptions<T, Z, A, M>>): TelemetryCache {
	//fill default direct action
	opts.direct = [
		...opts.direct || [],
		{ type: "*", zone: "*", action: "*" } as unknown as DirectTelemetry<T, Z, A, M>
	];

	return {
		synthetic: synth.preprocess<T, Z, A, M>((opts.synthetics || []) as Array<SyntheticTelemetry<any, any, any, any>>),
		direct: drct.preprocess<T, Z, A, M>((opts.direct || []) as Array<DirectTelemetry<any, any, any, any>>),
		network: (opts.network || []).slice(),
		history: {}
	};
}