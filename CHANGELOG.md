# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.3] - 2022-07-20
### Changed
 - Update version of servant

## [1.0.2] - 2021-11-10
### Changed
 - Update links in package.json

## [1.0.1] - 2021-11-10
### Added
 - First implementation of cuaatt package
 - Added react framework support
 - Added tracker gor Google Analytics
 - Documentation


[Unreleased]: https://gitlab.com/stanislavhacker/cuaatt/-/compare/1.0.3...master
[1.0.3]: https://gitlab.com/stanislavhacker/envfull/compare/1.0.2...1.0.3
[1.0.2]: https://gitlab.com/stanislavhacker/envfull/compare/1.0.1...1.0.2
[1.0.1]: https://gitlab.com/stanislavhacker/envfull/compare/init...1.0.1