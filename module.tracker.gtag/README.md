@cuaatt/tracker-gtag
======

### What is it?

This is simple Google Analytics tracker package for `@cuaatt/core` or `@cuaatt/rect` telemetry. You can easily set up middleware to send all telemetry messages that you want into Google Analytics service.

> **This is only Google Analytics tracker package. All necessary settings and principles of telemetry are described in [`@cuaatt/core` package][core]. So if you want to start using telemetry, try to read this documentation first and then continue here.**


### Install plain

In case that you don't want to use **react** do with [npm](https://npmjs.org):

```
npm install @cuaatt/core @cuaatt/tracker-gtag
```

### Install react

In case that you want to use **react** do with [npm](https://npmjs.org):

```
npm install @cuaatt/react @cuaatt/tracker-gtag
```

### How to initialize tracker?

Initialize tracker is really simple. It's basically middleware function that is provided as handler into telemetry. This tracker gets some options specific for Google Analytics service and original handler. This handler has only one difference. If returns `true`, this message will be sent on GA server, if returns `false`, this message will be ignored.

```typescript
import init from "@cuaatt/core";
import { trackerGtag } from "@cuaatt/tracker-gtag";

const Events = ["click"] as const; //list of all events that we want to watch
type Zone = "zone"; //list of zones that will be on app
type Action = "action"; //list of actions that will be on app
type Telemetry = Action | "telemetry"; //list of all telemetries that will be spawned and created by telemetry

const handler = (message) => {
    //process messages, if you want to send telemetry on server, use return true, false to ignore
    return true;
};

const tel = init<Zone, Action, Telemetry, typeof Events[number]>(
    trackerGtag({
        id, ... //tracker options
    }, handler), //tracker function for handling telemetry messages 
    (defaultValues) => ({ ...defaultValues }) //options for telemetry
);
tel.dispose(); //destory and clear telemetry
```

[TrackerGTag]: #trackergtag
### `TrackerGTag`

Settings object for telemetry.

- **`id: string;`**

This is only one required property. Its id that is provided by Google Analytics and have id pattern `G-********`.

- **`script?: boolean;`**

This property, that says if you want to append script with Google Analytics source library into page. Default value is **`true`**, but if you have already script included in page, you can set this property on `false`and this module will count that you already have it.

- **`gtag?: typeof gtag;`**

This property is basically gtag function from Google Analytics example. If you are not provided it, tracked will automatically create it and you don't need to do anything. But in case that you use this function already in app and need it to call from other places, you can provide it and tracker will use it.

**Example of usage if you are already Google Analytics set**

```typescript
function gtag(){
	dataLayer.push(arguments);
}

gtag('js', new Date());
gtag('config', id);
gtag('set', {
	'country': 'US',
	'currency': 'USD'
});

trackerGtag({ id, gtag, script: false }, handler);
```

### Donate me 😉

| QR | Paypal |
| ------ | ------ |
| ![](https://gitlab.com/uploads/-/system/personal_snippet/1929487/66399a49a06fa8eb9a0758b8673758c5/qr_sh.png) | [![](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DUT8W343NVGQQ&source=url) |


### License

MIT - [MIT License](https://spdx.org/licenses/MIT.html)

[core]: https://gitlab.com/stanislavhacker/cuaatt/-/blob/master/module.core/README.md