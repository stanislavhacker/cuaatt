import {trackerGtag} from "../src";
import {TelemetryMessage, TelemetryResolve} from "@cuaatt/core";

declare namespace window {
	let dataLayer: Array<any> | undefined;
}

function createMessage(zones: Array<string> = [], action: string = "", telemetry: string = "", resolve: TelemetryResolve = TelemetryResolve.Global): TelemetryMessage<any, any, any, any> {
	return {
		time: { start: new Date().getTime(), end: new Date().getTime() },
		url: "http://localhost:8080",
		params: {},
		zones,
		action,
		telemetry,
		resolve
	}
}

describe("google analytics tracker", () => {
	let handler;

	beforeEach(() => {
		window.dataLayer = [];
		handler = jasmine.createSpy("handler").and.callFake(() => true);
	});

	it("try init with invalid id", () => {
		expect(() => trackerGtag({ id: "" }, handler)).toThrowError(``);
	});

	it("init, send messages and destroy", () => {
		expect(window.dataLayer).toEqual([]);
		const start = document.head.querySelectorAll("script").length;
		expect(document.head.querySelectorAll("script").length).toBe(start);

		const tracker = trackerGtag({ id: "G-TEST" }, handler);

		expect(document.head.querySelectorAll("script").length).toBe(start + 1);
		tracker(createMessage(["main"], "action", "telemetry"));

		expect(window.dataLayer!.length).toEqual(3);
		expect(Array.prototype.slice.apply(window.dataLayer![2])).toEqual([
			"event",
			"telemetry",
			{ zones: [ 'main' ], url: 'http://localhost:8080', duration: jasmine.anything() }
		]);
		tracker.dispose!();

		expect(document.head.querySelectorAll("script").length).toBe(start);
	});

	it("init, send messages and destroy without script include", () => {
		expect(window.dataLayer).toEqual([]);
		const start = document.head.querySelectorAll("script").length;
		expect(document.head.querySelectorAll("script").length).toBe(start);

		const tracker = trackerGtag({ id: "G-TEST", script: false }, handler);

		expect(document.head.querySelectorAll("script").length).toBe(start);
		tracker(createMessage(["main"], "action", "telemetry"));

		expect(window.dataLayer!.length).toEqual(3);
		expect(Array.prototype.slice.apply(window.dataLayer![2])).toEqual([
			"event",
			"telemetry",
			{ zones: [ 'main' ], url: 'http://localhost:8080', duration: jasmine.anything() }
		]);
		tracker.dispose!();

		expect(document.head.querySelectorAll("script").length).toBe(start);
	});

	it("init, send messages and destroy without script include and custom gtag function", () => {
		const gtag = jasmine.createSpy("gtag");

		expect(window.dataLayer).toEqual([]);
		const start = document.head.querySelectorAll("script").length;
		expect(document.head.querySelectorAll("script").length).toBe(start);

		const tracker = trackerGtag({ id: "G-TEST", script: false, gtag }, handler);

		expect(document.head.querySelectorAll("script").length).toBe(start);
		tracker(createMessage(["main"], "action", "telemetry"));

		expect(window.dataLayer!.length).toEqual(0);
		expect(gtag).toHaveBeenCalledOnceWith('event', 'telemetry', {
			zones: [ 'main' ],
			url: 'http://localhost:8080',
			duration: jasmine.anything()
		});
		tracker.dispose!();

		expect(document.head.querySelectorAll("script").length).toBe(start);
	});

});