import {TelemetryHandler} from "@cuaatt/core";

const IdPrefix = "G-";

declare namespace window {
	let dataLayer: Array<any> | undefined;
}

type ArgumentTypes<T> = T extends (... args: infer U ) => infer R ? U: never;
type ReturnNew<T, TNewReturn> = (...a: ArgumentTypes<T>) => TNewReturn;

export type TelemetryHandlerCondition<T, Z, A, M> = ReturnNew<TelemetryHandler<T, Z, A, M>, boolean>;
export type TrackerGTag = {
	id: string;
	script?: boolean;
	gtag?: typeof gtag;
}

export function trackerGtag<T, Z, A, M>(
	{ id, gtag, script = true }: TrackerGTag,
	handler: TelemetryHandlerCondition<T, Z, A, M> = (rec) => Boolean(rec)):
	TelemetryHandler<T, Z, A, M> {
	//validate
	validateId(id);
	//initialize tag if not disabled
	const dispose = createScript(id, script);
	//create or get
	const fn = createGtag(id, gtag);

	const tracker = (record) => {
		//call handler
		const handle = handler(record);
		if (handle) {
			const duration = record.time.end - record.time.start;
			fn("event", String(record.telemetry || record.action), {
				zones: record.zones,
				url: record.url,
				duration: duration || 0,
				//propagate all other values
				...record.params
			});
		}
	};
	tracker.dispose = () => dispose();
	return tracker;
}

function validateId(id: string) {
	if (!id || id.indexOf(IdPrefix) !== 0) {
		throw new Error(`Provided google tracking id is not valid! Need to start with "${IdPrefix}" prefix.`);
	}
}

function createScript(id: string, create: boolean): () => void {
	if (create) {
		const script = document.createElement("script");
		script.src = `https://www.googletagmanager.com/gtag/js?id=${id}`;
		script.async = true;

		document.head.appendChild(script);

		return () => {
			document.head.removeChild(script);
		}
	}
	return () => void(0);
}

function createGtag(id: string, fn?: typeof gtag): typeof gtag {
	if (fn) {
		return fn;
	}

	window.dataLayer = window.dataLayer || [];
	function gtagFn(...args) {
		window.dataLayer!.push(arguments);
	}
	gtagFn('js', new Date());
	gtagFn('config', id);
	return gtagFn;
}