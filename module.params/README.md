@cuaatt/params
======

### What is it?

Module `@cuaatt/params` is used to composing and parsing attributes values from telemetry param string pattern. Module can be used separately, but it's included inside `@cuaatt/react` and `@cuaatt/core`. This module also contains weak map storage, that is used to store complex values inside telemetry attribute without strong referencing.

### Install

Do with [npm](https://npmjs.org):

```
npm install @cuaatt/params
```

## Composer

Composer is used to compose string attribute value from object and values that you can define. Composer use fluent interface to set and build parameter. In `@cuaatt/react` this module is used automatically and it's not necessary to use it directly.

### `compose(): Composer` - init and use it

This method is used to initialize composer and return fluent interface to enable composing. You can see this on example below.

```typescript
import { compose, Composer } from "@cuaatt/params";

const attributeValue = compose()
    .value("value")
    .values({ id: "test", value: true })
    .text()
    .attributes(["id", "href"])
    .build();
```

### `.build(): string` - build string value and return it

This method create final string value for telemetry param from data that are provided through fluent interface. This string value is special pattern for telemetry and can be parsed by `Parser`.

```typescript
import { compose, Composer } from "@cuaatt/params";

const attributeValue = ... //code from previous example

console.log(attributeValue);
//result: "[attr=id,href][val:s:id='test'][val:b:value='true'][text][val:s:value='value']"
```

### `.value(value: string): Composer` - set single value

This method is used to sent single string value to telemetry. 

> **Caveat**:
> 
> Do not use `.value()` with combination of `.values()`. This is not recommended because you can accidentally rewrite values. If you have one value, use `.value()`. In other cases use `values(...)` method.

```typescript
import { compose, Composer } from "@cuaatt/params";

const attributeValue = compose().value("test").build();

console.log(attributeValue);
//result: "[val:s:value='test']"
```

### `.values(values: Object): Composer` - set values from object

This method set values from object to telemetry param. Inside this object can be simple values (`string`, `boolean` or `number`) that are saved directly into parameter pattern. **Do not use this for long list of key values because you get really long attribute value. Use complex values instead of this.**

If value of object is a complex object, this module save this value into WeakMap storage and create ref identifier into attribute. After deserialize this value is read from storage and returned. This is much better approach because you get small size of attribute value.

```typescript
import { compose, Composer } from "@cuaatt/params";

const attributeValueWithSimpleValues = compose().values({
    id: "id",
    valid: true,
    count: 1
}).build();

console.log(attributeValueWithSimpleValues);
//result: "[val:s:id='id'][val:b:valid='true'][val:n:count='1']"
```

```typescript
import { compose, Composer } from "@cuaatt/params";

const attributeValueWithComplexValues = compose().values({
    id: "id",
    complex: { ... }
}).build();

console.log(attributeValueWithComplexValues);
//result: "[val:s:id='id'][ref:complex=0.e1d2frtr41e1]"
```

### `.text(): Composer` - use text content from this element

These options take text content from element with this attribute and include it into params output. After parsing attribute value this text is loaded and send into telemetry as params.

```typescript
import { compose, Composer } from "@cuaatt/params";

const attributeValue = compose().text().build();

console.log(attributeValue);
//result: "[text]"
```

### `.attributes(attrs: Array<string>): Composer` - use values from attributes

These options take value of all defined attributes and include it into params output. After parsing attribute value this attributes values are loaded and send into telemetry as params.

```typescript
import { compose, Composer } from "@cuaatt/params";

const attributeValue = compose().attributes(["id", "href"]).build();

console.log(attributeValue);
//result: "[attr=id,href]"
```

## Parser

Parser is used to parse telemetry params pattern string attribute from dom element. Parse use simple interface to parse provided string. In `@cuaatt/react` this module is used automatically and it's not necessary to use it directly.

### `parse(el: Element, value: string): Params` - parse params from element and params value

This method is used to parse data and return Params object with loaded values. Parse just simply get dom element where params attribute is used and value of this attribute and return object containing all parsed info. Everything that is created by `Composer` can be parsed by this method. You can see this on example below.

```typescript
import { parse, Params } from "@cuaatt/params";

const el = document.createElement("a");
el.setAttribute("id", "test-id");
el.setAttribute("href", "https://www.seznam.cz");
el.textContent = "Seznam.cz";
const params = parse(el, "[attr=id,href][text]");
console.log(params);
//results: {
// id: "test-id",
// href: "https://www.seznam.cz",
// text: "Seznam.cz",
//}
```

### Donate me 😉

| QR | Paypal |
| ------ | ------ |
| ![](https://gitlab.com/uploads/-/system/personal_snippet/1929487/66399a49a06fa8eb9a0758b8673758c5/qr_sh.png) | [![](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DUT8W343NVGQQ&source=url) |


### License

MIT - [MIT License](https://spdx.org/licenses/MIT.html)