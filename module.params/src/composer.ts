import {AttrsBuild, ValueBuild, TextBuild, RefBuild} from "./const";
import {toCache, supported} from "./cache/cache";

type ComposerState = {
	attributes: Array<string>;
	text: boolean;
	value: string;
	values: { [key: string]: Object };
}

export type Composer = {
	attrs: (attributes: Array<string>) => Composer;
	values: (values: { [key: string]: Object }) => Composer;
	value: (value: string) => Composer;
	text: () => Composer;
	build: () => string;
}

export function compose(): Composer {
	const composer = {} as Composer;
	const state = createState();

	composer.attrs = (attributes) => {
		state.attributes = attributes.slice();
		return composer;
	};
	composer.value = (value) => {
		state.value = value;
		return composer;
	};
	composer.values = (values) => {
		state.values = values;
		return composer;
	};
	composer.text = () => {
		state.text = true;
		return composer;
	};

	composer.build = () => build(state);

	return composer;
}

function build(state: ComposerState): string {
	const data = [] as Array<string>;

	if (state.attributes.length) {
		data.push(AttrsBuild(state.attributes));
	}
	Object.keys(state.values).forEach((key) => {
		data.push(buildValue(key, state.values[key]));
	});
	if (state.text) {
		data.push(TextBuild());
	}
	if (state.value) {
		if (data.length === 0) {
			data.push(state.value);
		} else {
			data.push(ValueBuild("value", state.value));
		}
	}

	state.values = {};
	state.value = "";
	state.text = false;
	state.attributes = [];

	return data.join("");
}

function buildValue(id: string, value: Object): string {
	if (typeof value === "object" && !supported) {
		throw new Error(`Can not use complex objects in telemetry. WeakRef is not supported by your browser.`);
	}
	if (typeof value === "object") {
		return RefBuild(id, toCache(value));
	}
	return ValueBuild(id, value);
}

function createState(): ComposerState {
	return {
		attributes: [],
		text: false,
		value: "",
		values: {}
	};
}