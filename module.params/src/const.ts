
export const Attrs = "attr";
export const Text = "text";
export const Value = "val";
export const Ref = "ref";

export const StringType = "s" as const;
export const BooleanType = "b" as const;
export const NumberType = "n" as const;
export const ObjectType = "o" as const;

export const Types = [StringType, BooleanType, NumberType, ObjectType] as const;

export const AttrsRegEx = new RegExp(`\\[(${Attrs})=([\\s\\S]*?)]`, "g");
export const TextRegEx = new RegExp(`\\[(${Text})]`, "g");
export const Value1RegEx = new RegExp(`\\[(${Value}):([${Types.join("")}]):([a-zA-Z0-9]*?)=\"([\\s\\S]*?)\"]`, "g");
export const Value2RegEx = new RegExp(`\\[(${Value}):([${Types.join("")}]):([a-zA-Z0-9]*?)='([\\s\\S]*?)']`, "g");
export const RefRegEx = new RegExp(`\\[(${Ref}):([a-zA-Z0-9]*?)=([\\s\\S]*?)]`, "g");

export const ParamsRegEx = [AttrsRegEx, TextRegEx, Value1RegEx, Value2RegEx, RefRegEx];

export const AttrsBuild = (items: Array<string>) => `[${Attrs}=${items.join(",")}]`;
export const ValueBuild = (id: string, val: string | number | boolean | null) => String(val).indexOf("'") >= 0 ?  `[${Value}:${resolveType(val)}:${id}="${val}"]` : `[${Value}:${resolveType(val)}:${id}='${val}']`;
export const RefBuild = (id: string, ref: string) => `[${Ref}:${id}=${ref}]`;
export const TextBuild = () => `[${Text}]`;

function resolveType(val: string | number | boolean | null) {
	if (typeof val === "string") {
		return StringType;
	}
	if (typeof val === "number") {
		return NumberType;
	}
	if (typeof val === "boolean") {
		return BooleanType;
	}
	return ObjectType;
}

export function parseType(val: string, type: typeof Types[number]) {
	if (type === StringType) {
		return String(val);
	}
	if (type === NumberType) {
		return parseFloat(val);
	}
	if (type === BooleanType) {
		return ["true", "1", "yes", "y"].indexOf(String(val).toLowerCase()) >= 0;
	}

	if (val === "null") {
		return null;
	}
	if (val === "undefined") {
		return undefined;
	}
	return val;
}