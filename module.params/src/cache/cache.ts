import {TwoWayWeakMapSupported, TwoWayWeakMap} from "./twoWayWeakMap";

type Cache = {
	supported: boolean;
	toCache: (object: Object) => string;
	fromCache: (id: string) => object | undefined;
}

//IDS
let counter = 0;
function generateId() {
	const id = Math.random().toString(36) + counter;
	counter++;
	return id;
}

//TRY GET
function tryGet(map: TwoWayWeakMap<Object, string>, object: Object): string {
	let id = map.get(object);

	if (!id) {
		id = generateId();
		map.set(object, id);
	}
	return id;
}

function createCache(): Cache {
	const api: Cache = {
		supported: TwoWayWeakMapSupported,
		fromCache: () => undefined,
		toCache: () => "",
	};

	if (TwoWayWeakMapSupported) {
		const map = new TwoWayWeakMap<Object, string>();
		api.toCache = (object: Object): string => tryGet(map, object);
		api.fromCache = (id: string): object | undefined => map.keyFrom(id) || undefined
	}

	return api;
}

export const {supported, fromCache, toCache} = createCache();