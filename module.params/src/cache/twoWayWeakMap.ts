export class TwoWayWeakMap<K extends object, V> {
	private readonly forwardMap: WeakMap<K, V>;
	private readonly reverseMap: Map<V, WeakRef<K>>;
	private timer: number | undefined;
	private pooling: number = 5000;

	constructor(iterable?: Readonly<Array<[K, V]>>) {
		this.forwardMap = new WeakMap<K, V>();
		this.reverseMap = new Map<V, WeakRef<K>>();
		this.timer = window.setInterval(() => cleanup(this, this.reverseMap), this.pooling);
		fill(this, iterable);
	}

	set(key: K, value: V): TwoWayWeakMap<K, V> {
		this.forwardMap.set(key, value);
		this.reverseMap.set(value, new WeakRef(key));
		return this;
	}

	get(key: K): V | undefined {
		return this.forwardMap.get(key);
	}

	delete(key: K): boolean {
		return this.forwardMap.delete(key);
	}

	keyFrom(value: V): K | undefined {
		const ref = this.reverseMap.get(value);
		return keyAndCleanup(this.reverseMap, ref, value);
	}
}

function fill<K extends object, V>(map: TwoWayWeakMap<K, V>, iterable?: Readonly<Array<[K, V]>>) {
	if (iterable) {
		for (const [key, value] of iterable ) {
			map.set(key, value);
		}
	}
}

function cleanup<K extends object, V>(map: TwoWayWeakMap<K, V>, reverseMap: Map<V, WeakRef<K>>) {
	reverseMap.forEach((keyRef, value) => keyAndCleanup(reverseMap, keyRef, value));
}

function keyAndCleanup<K extends object, V>(reverseMap: Map<V, WeakRef<K>>, keyRef: WeakRef<K> | undefined, value: V): K | undefined {
	const key = keyRef && keyRef.deref();
	if (!key) {
		reverseMap.delete(value);
	}
	return key;
}

export const TwoWayWeakMapSupported = typeof WeakMap !== "undefined" && typeof WeakRef !== "undefined";

interface WeakRef<T extends object> {
	readonly [Symbol.toStringTag]: "WeakRef";
	deref(): T | undefined;
}
interface WeakRefConstructor {
	readonly prototype: WeakRef<any>;
	new<T extends object>(target?: T): WeakRef<T>;
}
declare var WeakRef: WeakRefConstructor;