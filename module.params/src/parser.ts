import {ParamsRegEx, Attrs, Value, Ref, Text, Types, parseType, ObjectType, StringType} from "./const";
import {fromCache, supported} from "./cache/cache";

export type TextValueParams = {
	text?: string;
}
export type MapValuesParams = {
	[key: string]: object | number | boolean | string | null | undefined;
}
export type Params = MapValuesParams & TextValueParams;

export function parse(el: Element, value: string): Params {
	let data = {};

	//not defined
	if (!value) {
		return data;
	}
	//iterate all
	ParamsRegEx.forEach((reg) => {
		let array;
		//reset
		reg.lastIndex = 0;
		//iterate all
		while (array = reg.exec(value)) {
			data = {
				...data,
				...processParam(el, array)
			}
		}
	});
	//not match to regex, use whole value
	if (Object.keys(data).length === 0) {
		data = {
			...data,
			...createValue("value", StringType, value)
		}
	}
	return data;
}

function processParam(el: Element, array: RegExpExecArray): Params {
	const [, what] = array;

	switch (what) {
		case Attrs: {
			const [,, val] = array;
			return createAttributes(el, val);
		}
		case Value: {
			const [,, type, id, val] = array;
			return createValue(id, type as any, val);
		}
		case Text: {
			return createText(el);
		}
		case Ref: {
			if (!supported) {
				throw new Error(`Can not use complex objects in telemetry. WeakRef is not supported by your browser.`);
			}
			const [,, id, val] = array;
			return createValue(id, ObjectType, fromCache(val));
		}
		default:
			return {};
	}
}

function createAttributes(el: Element, val: string): MapValuesParams {
	const attributes = val.split(",").filter(Boolean);
	const data: MapValuesParams = {};

	attributes.forEach((attr) => {
		data[attr] = el.getAttribute(attr) || null;
	});
	return data;
}

function createText(el: Element): TextValueParams {
	return {
		text: el.textContent || ""
	};
}

function createValue(id: string, type: typeof Types[number], value: any): MapValuesParams {
	return {
		[id]: parseType(value, type)
	};
}