import {compose} from "../src";
import * as cache from "../src/cache/cache";

describe("composer", () => {

	it("empty", () => {
		const comp = compose();
		expect(comp.build()).toBe("");
	});

	it("with two attributes", () => {
		const comp = compose()
			.attrs(["id", "href"]);
		expect(comp.build()).toBe("[attr=id,href]");
	});

	it("with one attribute", () => {
		const comp = compose()
			.attrs(["id"]);
		expect(comp.build()).toBe("[attr=id]");
	});

	it("with text", () => {
		const comp = compose()
			.text();
		expect(comp.build()).toBe("[text]");
	});

	it("with value", () => {
		const comp = compose()
			.value("value");
		expect(comp.build()).toBe("value");
	});

	it("with simple values", () => {
		const comp = compose()
			.values({ id: "id", valid: true, count: 3 });
		expect(comp.build()).toBe("[val:s:id='id'][val:b:valid='true'][val:n:count='3']");
	});

	it("with complex value", () => {
		const obj = { obj: true };
		const comp = compose()
			.values({ id: "id", complex: obj });

		if (cache.supported) {
			expect(comp.build()).toMatch(/\[val:s:id='id']\[ref:complex=[\s\S]*?]/g);
		} else
			expect(() => comp.build())
				.toThrowError("Can not use complex objects in telemetry. WeakRef is not supported by your browser.");
	});

	it("with all simple values", () => {
		const comp = compose()
			.attrs(["id", "href"])
			.text()
			.value("test")
			.values({ id: true, count: 2 });
		expect(comp.build()).toBe("[attr=id,href][val:b:id='true'][val:n:count='2'][text][val:s:value='test']");
	});

	it("with value and values", () => {
		const comp = compose()
			.value("test")
			.values({ id: true, count: 2 });
		expect(comp.build()).toBe("[val:b:id='true'][val:n:count='2'][val:s:value='test']");
	});

	it("with value and values same key", () => {
		const comp = compose()
			.value("1")
			.values({ value: 2 });
		expect(comp.build()).toBe("[val:n:value='2'][val:s:value='1']");
	});

});