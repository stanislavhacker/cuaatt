import {parse} from "../src";
import * as cache from "../src/cache/cache";

describe("parser", () => {

	it("empty", () => {
		const el = document.createElement("div");
		const res = parse(el, "");
		expect(res).toEqual({});
	});

	it("with two attributes and empty element", () => {
		const el = document.createElement("div");
		const res = parse(el, "[attr=id,href]");
		expect(res).toEqual({
			id: null,
			href: null
		});
	});

	it("with two attributes and filled element", () => {
		const el = document.createElement("div");
		el.setAttribute("id", "this-is-id");
		el.setAttribute("href", "/#href");
		const res = parse(el, "[attr=id,href]");
		expect(res).toEqual({
			id: 'this-is-id',
			href: '/#href'
		});
	});

	it("with one attribute", () => {
		const el = document.createElement("div");
		el.setAttribute("id", "this-is-id");
		const res = parse(el, "[attr=id]");
		expect(res).toEqual({
			id: 'this-is-id'
		});
	});

	it("with text", () => {
		const el = document.createElement("div");
		el.textContent = "This is text content";
		const res = parse(el, "[text]");
		expect(res).toEqual({
			text: 'This is text content'
		});
	});

	it("with value", () => {
		const el = document.createElement("div");
		el.textContent = "This is text content";
		const res = parse(el, "value");
		expect(res).toEqual({
			value: 'value'
		});
	});

	it("with simple values", () => {
		const el = document.createElement("div");
		const res = parse(el, "[val:s:id='id'][val:b:valid='true'][val:n:count='3']");
		expect(res).toEqual({
			id: 'id',
			valid: true,
			count: 3
		});
	});

	it("with complex value and non existing store data", () => {
		const el = document.createElement("div");

		if (cache.supported) {
			const res = parse(el, "[val:s:id='id'][ref:complex=refId]");
			expect(res).toEqual({
				id: 'id',
				complex: undefined
			});
		} else {
			expect(() => parse(el, "[val:s:id='id'][ref:complex=refId]"))
				.toThrowError("Can not use complex objects in telemetry. WeakRef is not supported by your browser.");
		}
	});

	it("with complex value and existing store data", () => {
		spyOn(cache, "fromCache").and.returnValue({ val: true, ref: "testId" });
		const el = document.createElement("div");

		if (cache.supported) {
			const res = parse(el, "[val:s:id='id'][ref:complex=refId]");
			expect(res).toEqual({
				id: 'id',
				complex: {
					val: true,
					ref: 'testId'
				}
			});
		} else {
			expect(() => parse(el, "[val:s:id='id'][ref:complex=refId]"))
				.toThrowError("Can not use complex objects in telemetry. WeakRef is not supported by your browser.");
		}
	});

	it("with all simple values", () => {
		const el = document.createElement("div");
		el.setAttribute("id", "elementId");
		el.setAttribute("href", "/#location");
		el.textContent = "Text content";
		const res = parse(el, "[attr=id,href][val:b:id='true'][val:n:count='2'][text][val:s:value='test']");
		expect(res).toEqual({
			id: true, //NOTE: Element id is rewrite
			href: '/#location',
			text: 'Text content',
			count: 2,
			value: 'test',
		});
	});

	it("with all simple values no rewrite", () => {
		const el = document.createElement("div");
		el.setAttribute("id", "elementId");
		el.setAttribute("href", "/#location");
		el.textContent = "Text content";
		const res = parse(el, "[attr=id,href][val:b:valid='true'][val:n:count='2'][text][val:s:value='test']");
		expect(res).toEqual({
			id: "elementId",
			valid: true,
			href: '/#location',
			text: 'Text content',
			count: 2,
			value: 'test',
		});
	});

	it("with value and values", () => {
		const el = document.createElement("div");
		const res = parse(el, "[val:b:id='true'][val:n:count='2'][val:s:value='test']");
		expect(res).toEqual({
			id: true,
			count: 2,
			value: 'test'
		});
	});

	it("with value and values same key", () => {
		const el = document.createElement("div");
		const res = parse(el, "[val:n:value='2'][val:s:value='1']");
		expect(res).toEqual({
			value: '1' //NOTE: Last value is used
		});
	});

});