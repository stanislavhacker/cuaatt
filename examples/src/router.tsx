import * as React from "react";
import * as ReactDOM from "react-dom";
import { HashRouter as Router, Switch, Route } from "react-router-dom";

import {Plain} from "./plain";
import {Main} from "./main";
import {ForReact} from "./react";

import "./index.css";
import { Paths } from "./routes";

export function render(element: Element) {
	ReactDOM.render((
		<Router>
			<div>
				<Switch>
					<Route path={[Paths["/react"], Paths["/react/:tracker"]]} exact children={<ForReact />} />
					<Route path={[Paths["/plain"], Paths["/plain/:tracker"]]} exact children={<Plain />} />
					<Route path={Paths["/"]} children={<Main />} />
				</Switch>
			</div>
		</Router>
	), element);
}