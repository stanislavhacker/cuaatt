import * as React from "react";
import {useState} from "react";
import {Link, useRouteMatch, useParams} from "react-router-dom";

import {Example, Action as ActionInfo} from "../components";

import {Telemetry, Zone, Action} from "./createReactTelemetry";
import css from "./react.css";

export const ForReact: React.FC = () => {
	const { tracker } = useParams<{ tracker: string }>();
	const { path } = useRouteMatch();
	const [zone1Show, showZone1] = useState(false);
	const [zone2Show, showZone2] = useState(false);

	const [complex] = useState<any>(() => ({ name: "Complex object", valid: true }));

	return (
		<Telemetry tracker={tracker}>
			<Zone name="react-main" el={<div className={css.ReactPage}  />}>
				<h1>React example</h1>
				<p>
					This a simple example to use react framework <code>@cuaatt/react</code> telemetry. Every responses from telemetry are log into <b>browser console</b> so you can open developer tools to see all relevant messages that are triggered by telemetry. In react mode telemetry is created as telemetry provider that you can view in snippet <Action name="open-external-link" el={<a href="https://gitlab.com/stanislavhacker/cuaatt/-/snippets/2198183" target="_blank" />}>Plain: Init telemetry</Action>. There is necessary to define attribute name for zone, action and also params. There need to be also defined what events do you need to listen. <b>Keep on mind that lots of events make telemetry slow and there will be plenty of messages triggered.</b>
				</p>
				<p>
					There need to be also defined direct, network and synthetic telemetry events. This is an optional settings because <code>@cuaatt/react</code> can work without this settings. If you not provide settings for direct, network or synthetic events, every action will be reported as global and telemetry name will be name of action. How to defined specific direct, network and synthetics telemetry is described in documentation.
				</p>

				{tracker === "gtag" && (
					<>
						<h3>Using react telemetry with <u>Google Analytics</u> tracker.</h3>
						<p>
							Now you are using telemetry with <b>Google Analytics</b>. That means that every telemetry is send into Google Analytics service. Tracker is really easy to use and you are able to implement your own tracker to send data to your preffered service.
						</p>
					</>
				)}

				<h3>Actions without params</h3>
				<p>In this example every link on button spawn direct telemetry action <ActionInfo action="open-normal-link" /> without any params defined.</p>
				<div className={css.ReactPageList}>
					<Example code={`<Action name="open-normal-link" el={<a href="#" />}>Simple link</Action>`}>
						<Action name="open-normal-link" el={<Link to={path} />}>Simple link</Action>
					</Example>
					<Example code={`<Action name="open-normal-link" el={<button />}>Button</Action>`}>
						<Action name="open-normal-link" el={<button />}>Button</Action>
					</Example>
				</div>

				<h3>Actions with params</h3>
				<p>In this example every link on button spawn direct telemetry action <ActionInfo action="open-normal-link" /> with params defined. There are more types of params and there is predefined way to use it. By using <code>@cuaatt/react</code> you are able to simply create param value and also store complex values and objects. Telemetry use WeakMaps to ensure good work without references. More info is available in documentation.</p>
				<div className={css.ReactPageList}>
					<Example code={`<Action name="open-normal-link" attributes={["href"]} params={{ value: "Cool link" }} el={<a href="#" />}>Simple link</Action>`}>
						<Action name="open-normal-link" attributes={["href"]} params={{ value: "Cool link" }} el={<Link to={path} />}>Simple link</Action>
					</Example>
					<Example code={`<Action name="open-normal-link" attributes={["id", "title"]} el={<button id="test" title="Title" />}>Button</Action>`}>
						<Action name="open-normal-link" attributes={["id", "title"]} el={<button id="test" title="Title" />}>Button</Action>
					</Example>
					<Example code={`<Action name="open-normal-link" params={{ complex: complex }} el={<button />}>Button with complex</Action>`}>
						<Action name="open-normal-link" params={{ complex: complex }} el={<button />}>Button with complex</Action>
					</Example>
				</div>

				<h3>Zones</h3>
				<p>Zone can be managed in way of zone appear and disappear from page. Telemetry is not able to work with zones that are only hidden by <u>visibility change</u> or <u>display "none"</u>. Here you can show zones and look on console to understand zones features.</p>
				<div className={css.ReactPageList}>
					{zone1Show && (
						<Zone name="react-zone-1" el={<div className={css.PageZone} />} value="This is zone 1">
							<p>This is content of zone 1. As you can see in browser console, zone is reported as that appear. There are some button with same action <ActionInfo action="open-normal-link" /> as above, but these buttons are inside zone so can be reported in nested zone! This zone also trigger synthetic telemetry that tells how long you have stay in zone. Use hide button and look into console!</p>

							<div className={css.ReactPageList}>
								<Example code={`<Action name="open-normal-link" attributes={["href"]} value="Cool link" el={<a href="#" />}>Simple link</Action>`}>
									<Action name="open-normal-link" attributes={["href"]} value="Cool link" el={<Link to={path} />}>Simple link</Action>
								</Example>
								<Example code={`<Action name="open-normal-link" attributes={["id", "title"]} text el={<button id="test" title="Title" />}>Button</Action>`}>
									<Action name="open-normal-link" attributes={["id", "title"]} text el={<button id="test" title="Title" />}>Button</Action>
								</Example>
							</div>

							<button data-action="hide-zone" onClick={() => showZone1(!zone1Show)}>Hide zone 1</button>
						</Zone>
					)}
					{!zone1Show && (
						<Example code={`<Zone name="react-zone-1" el={<div />} value="This is zone 1">...</Zone>`}>
							<Action name="show-zone" el={<button onClick={() => showZone1(true)} disabled={zone1Show} />}>Show zone 1</Action>
						</Example>
					)}
					{zone2Show && (
						<Zone name="react-zone-2" el={<div className={css.PageZone}  />} attributes={["class"]}>
							<p>This is content of zone 2. As you can see in browser console, zone is reported as that appear. This zone contains another zone that is appear in some time as this zone. So as is showed, you are able to nested zones inside another zones!</p>
							<Zone name="react-zone-2-inner" el={<div className={css.PageZone}  />} attributes={["class"]} text>
								<p>This is content of zone 2 inner :)</p>
							</Zone>

							<Action name="hide-zone" el={<button onClick={() => showZone2(!zone2Show)} />}>Hide zone 2</Action>
						</Zone>
					)}
					{!zone2Show && (
						<Example code={`<Zone name="react-zone-2" el={<div className={css.PageZone}  />} attributes={["class"]}><Zone name="react-zone-2-inner" el={<div className={css.PageZone}  />} attributes={["class"]} text>...</Zone></Zone>`}>
							<Action name="show-zone" el={<button onClick={() => showZone2(true)} disabled={zone2Show} />}>Show zone 2</Action>
						</Example>
					)}
				</div>

				<h3>Request</h3>
				<p>Request can be also part of telemetry and it is possible to create telemetry on them. Clicking on these buttons you can see spawned <ActionInfo action="request-fail" /> or <ActionInfo action="request-ok" /> actions. There are also synthetics telemetry actions <ActionInfo action="request-success-duration" /> and <ActionInfo action="request-fail-duration" /> that are composed from click action and success or fail request action.</p>
				<div className={css.ReactPageList}>
					<Example code={`<Action name="open-external-link" el={<button onClick={() => runFetch("http://www.dsfsdfdsfsdf.cz")} />}>...</Action>`}>
						<Action name="open-external-link" el={<button onClick={() => runFetch("http://www.dsfsdfdsfsdf.cz")} />}>Invalid request by using "fetch"</Action>
					</Example>
					<Example code={`<Action name="open-external-link" el={<button onClick={() => runXmlHttpRequest("GET", "http://www.dsfsdfdsfsdf.cz")} />}>...</Action>`}>
						<Action name="open-external-link" el={<button onClick={() => runXmlHttpRequest("GET", "http://www.dsfsdfdsfsdf.cz")} />}>Invalid request by using "XmlHttpRequest"</Action>
					</Example>
					<Example code={`<Action name="open-external-link" el={<button onClick={() => runFetch(window.location.origin)} />}>...</Action>`}>
						<Action name="open-external-link" el={<button onClick={() => runFetch(window.location.origin)} />}>Valid request by using "fetch"</Action>
					</Example>
					<Example code={`<Action name="open-external-link" el={<button onClick={() => runXmlHttpRequest("GET", window.location.origin)} />}>...</Action>`}>
						<Action name="open-external-link" el={<button onClick={() => runXmlHttpRequest("GET", window.location.origin)} />}>Valid request by using "XmlHttpRequest"</Action>
					</Example>
				</div>
			</Zone>
		</Telemetry>
	);
}

function runFetch(url: RequestInfo, init?: RequestInit) {
	return fetch(url, init);
}

function runXmlHttpRequest(method: string, url: string, body?: Document | XMLHttpRequestBodyInit | null) {
	const req = new XMLHttpRequest();
	req.open(method, url);
	req.send(body);
}