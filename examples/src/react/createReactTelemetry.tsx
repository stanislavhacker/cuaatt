import {trackerGtag} from "@cuaatt/tracker-gtag";
import React, {useEffect} from "react";
import {createTelemetry, TelemetryZoneComponent, TelemetryActionComponent, TelemetryMessage, TelemetryResolve } from "@cuaatt/react";
import {reportTelemetry, reportInit, reportDispose} from "../helpers";

/** List of all events to listen on */
const TelemetryEvents = ["mousedown", "mouseup", "click", "keydown", "keypress", "keyup", "focus"] as const;

/** Define list of all actions available in app */
type Action = "open-normal-link" | "open-external-link" | "show-zone" | "hide-zone";
/** Define list of all zones available in app */
type Zones = "react-main" | "react-zone-1" | "react-zone-2" | "react-zone-2-inner";
/** Define list of all telemetry that can be created by combination of zones and actions */
type Telemetry = Action | "zone-appear" | "zone-disappear" | "zone-duration" | "request-fail" | "request-ok" | "request-success-duration" | "request-fail-duration";


const tel = createTelemetry<typeof TelemetryEvents[number], Zones, Action, Telemetry>();

export const Zone: TelemetryZoneComponent<Zones> = tel.Zone;
export const Action: TelemetryActionComponent<Action> = tel.Action;

export const Telemetry: React.FC<{ tracker?: string }> = ({ children, tracker }) => {
	useEffect(() => {
		reportInit("REACT", tracker);
		return () => reportDispose("REACT")
	}, []);

	return (
		<tel.TelemetryProvider options={{
			events: ["mousedown", "mouseup", "click", "keydown", "keypress", "keyup", "focus"] as const,
			direct: [
				{ type: "click", zone: "*", action: "open-normal-link" },
				{ type: "click", zone: "*", action: "open-external-link" },
				{ type: "click", zone: "*", action: "show-zone" },
				{ type: "click", zone: "*", action: "hide-zone" },
				{ type: "added", zone: "*", creates: "zone-appear" },
				{ type: "removed", zone: "*", creates: "zone-disappear" },
				{ type: "*", zone: "*", action: "*" }
			],
			network: [
				{ method: "GET", url: "*", status: [/^(4[0-9]{2})/, 0], creates: "request-fail" },
				{ method: "GET", url: "*", status: /^(2[0-9]{2})/, creates: "request-ok" }
			],
			synthetics: [
				{ start: { type: "added", zone: "react-zone-1" }, end: { type: "removed", zone: "react-zone-1" }, creates: "zone-duration" },
				{ start: { type: "click", action: "open-external-link" }, end: { type: "network", action: "request-ok" }, creates: "request-success-duration" },
				{ start: { type: "click", action: "open-external-link" }, end: { type: "network", action: "request-fail" }, creates: "request-fail-duration" }
			]
		}} handler={getTracker(tracker)}>{children}</tel.TelemetryProvider>
	)
};

function getTracker(tracker?: string) {
	switch (tracker) {
		case "gtag":
			return trackerGtag({ id: "G-1QZ67C86QBX" }, handle);
		default:
			return handle;
	}
}

function handle(info: TelemetryMessage<typeof TelemetryEvents[number], Zones, Action, Telemetry | Action>): boolean {
	const telemetry = info.telemetry || null;
	const zones = info.zones || [];
	const action = info.action || null;
	const params = info.params || null;
	const url = info.url;

	/** We dont need to report "global" resolved telemetry */
	if (info.resolve === TelemetryResolve.Global) {
		return false;
	}

	reportTelemetry(info.time.start, info.time.end, "REACT", url, telemetry, zones, action, params);
	return true;
}