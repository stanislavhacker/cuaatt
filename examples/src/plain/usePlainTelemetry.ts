import telemetry, {TelemetryMessage, TelemetryResolve} from "@cuaatt/core";
import { trackerGtag } from "@cuaatt/tracker-gtag";
import {useEffect} from "react";
import "console.style";

import {reportTelemetry, reportInit, reportDispose} from "../helpers";

/** Define telemetry zone attribute name */
const TelemetryZoneAttr = "data-zone";
/** Define telemetry action attribute name */
const TelemetryActionAttr = "data-action";
/** Define telemetry action attribute name */
const TelemetryParamsAttr = "data-params";
/** List of all events to listen on */
const TelemetryEvents = ["mousedown", "mouseup", "click", "keydown", "keypress", "keyup", "focus"] as const;

/** Define list of all actions available in app */
type Action = "open-normal-link" | "open-external-link" | "show-zone" | "hide-zone";
/** Define list of all zones available in app */
type Zone = "plain-main" | "plain-zone-1" | "plain-zone-2" | "plain-zone-2-inner";
/** Define list of all telemetry that can be created by combination of zones and actions */
type Telemetry = Action | "zone-appear" | "zone-disappear" | "zone-duration" | "request-fail" | "request-ok" | "request-success-duration" | "request-fail-duration";

/**
 * Define types for react, in this example we dont use cuaatt/react but only plain version of telemetry
 * that can be used everywhere. But for better experience we define types for html attributes. React version
 * of cuaatt will do this for you.
 **/
declare module "react" {
	interface HTMLAttributes<T> {
		[TelemetryActionAttr]?: Action;
		[TelemetryZoneAttr]?: Zone;
		[TelemetryParamsAttr]?: string;
	}
}

export function usePlainTelemetry(tracker?: string) {
	return useEffect(() => {
		reportInit("PLAIN", tracker);
		const tel = telemetry<Zone, Action, Telemetry, typeof TelemetryEvents[number]>(
			getTracker(tracker),
			(defaults) => {
				return {
					...defaults,
					actionAttributes: [TelemetryActionAttr],
					zoneAttributes: [TelemetryZoneAttr],
					paramsAttributes: [TelemetryParamsAttr],
					events: TelemetryEvents,
					direct: [
						{ type: "click", zone: "*", action: "open-normal-link" },
						{ type: "click", zone: "*", action: "open-external-link" },
						{ type: "click", zone: "*", action: "show-zone" },
						{ type: "click", zone: "*", action: "hide-zone" },
						{ type: "added", zone: "*", creates: "zone-appear" },
						{ type: "removed", zone: "*", creates: "zone-disappear" },
						{ type: "*", zone: "*", action: "*" }
					],
					network: [
						{ method: "GET", url: "*", status: [/^(4[0-9]{2})/, 0], creates: "request-fail" },
						{ method: "GET", url: "*", status: /^(2[0-9]{2})/, creates: "request-ok" }
					],
					synthetics: [
						{ start: { type: "added", zone: "plain-zone-1" }, end: { type: "removed", zone: "plain-zone-1" }, creates: "zone-duration" },
						{ start: { type: "click", action: "open-external-link" }, end: { type: "network", action: "request-ok" }, creates: "request-success-duration" },
						{ start: { type: "click", action: "open-external-link" }, end: { type: "network", action: "request-fail" }, creates: "request-fail-duration" }
					]
				}
			});
		return () => {
			reportDispose("PLAIN");
			tel.dispose();
		};
	}, []);
}

function getTracker(tracker?: string) {
	switch (tracker) {
		case "gtag":
			return trackerGtag({ id: "G-1QZ67C86QBX" }, handle);
		default:
			return handle;
	}
}

function handle(info: TelemetryMessage<typeof TelemetryEvents[number], Zone, Action, Telemetry | Action>): boolean {
	const telemetry = info.telemetry || null;
	const zones = info.zones || [];
	const action = info.action || null;
	const params = info.params || null;
	const url = info.url;

	/** We dont need to report "global" resolved telemetry */
	if (info.resolve === TelemetryResolve.Global) {
		return false;
	}

	reportTelemetry(info.time.start, info.time.end, "PLAIN", url, telemetry, zones, action, params);
	return true;
}