import * as React from "react";
import {useState} from "react";
import {useRouteMatch, Link, useParams} from "react-router-dom";
import { compose } from "@cuaatt/params";

import css from "./plain.css";
import {usePlainTelemetry} from "./usePlainTelemetry";
import {Example, Action} from "../components";

export const Plain: React.FC = () => {
	const { tracker } = useParams<{ tracker: string }>();
	const { path } = useRouteMatch();
	const [zone1Show, showZone1] = useState(false);
	const [zone2Show, showZone2] = useState(false);

	const [complex] = useState<any>(() => ({ name: "Complex object", valid: true }));
	usePlainTelemetry(tracker);

	return (
		<div className={css.PlainPage} data-zone="plain-main">
			<h1>Plain example</h1>
			<p>
				This a simple example to use plain <code>@cuaatt/core</code> telemetry. Every responses from telemetry are log into <b>browser console</b> so you can open developer tools to see all relevant messages that are triggered by telemetry. In plain mode telemetry can be created as simple code that you can view in snippet <a href="https://gitlab.com/stanislavhacker/cuaatt/-/snippets/2194339" target="_blank" data-action="open-external-link">Plain: Init telemetry</a>. There is necessary to define attribute name for zone, action and also params. There need to be also defined what events do you need to listen. <b>Keep on mind that lots of events make telemetry slow and there will be plenty of messages triggered.</b>
			</p>
			<p>
				There need to be also defined direct, network and synthetic telemetry events. This is an optional settings because <code>@cuaatt/core</code> can work without this settings. If you not provide settings for direct, network or synthetic events, every action will be reported as global and telemetry name will be name of action. How to defined specific direct, network and synthetics telemetry is described in documentation.
			</p>

			{tracker === "gtag" && (
				<>
					<h3>Using plain telemetry with <u>Google Analytics</u> tracker.</h3>
					<p>
						Now you are using telemetry with <b>Google Analytics</b>. That means that every telemetry is send into Google Analytics service. Tracker is really easy to use and you are able to implement your own tracker to send data to your preffered service.
					</p>
				</>
			)}

			<h3>Actions without params</h3>
			<p>In this example every link on button spawn direct telemetry action <Action action="open-normal-link" /> without any params defined.</p>
			<div className={css.PlainPageList}>
				<Example code={`<a href="http://some-url" data-action="open-normal-link">Simple link</a>`}>
					<Link to={path} data-action="open-normal-link">Simple link</Link>
				</Example>
				<Example code={`<button data-action="open-normal-link">Button</button>`}>
					<button data-action="open-normal-link">Button</button>
				</Example>
			</div>

			<h3>Actions with params</h3>
			<p>In this example every link on button spawn direct telemetry action <Action action="open-normal-link" /> with params defined. There are more types of params and there is predefined syntax to use it. More info is available in documentation.</p>
			<div className={css.PlainPageList}>
				<Example code={`<a href="http://some-url" data-action="open-normal-link" data-params="[attr=href][val:s:value='Cool link']">Simple link</a>`}>
					<Link to={path} data-action="open-normal-link" data-params="[attr=href][val:s:value='Cool link']">Simple link</Link>
				</Example>
				<Example code={`<button id="test" title="Title" data-action="open-normal-link" data-params="[attr=id,title][text]">Button</button>`}>
					<button id="test" title="Title" data-action="open-normal-link" data-params="[attr=id,title][text]">Button</button>
				</Example>
			</div>

			<h3>Actions with params builder</h3>
			<p>In this example param attribute is build by special module <code>@cuaatt/params</code>. By using this module you are able to simply create param value and also store complex values and objects. Telemetry use WeakMaps to ensure good work without references.</p>
			<div className={css.PlainPageList}>
				<Example code={`<button id="test" title="Title" data-action="open-normal-link" data-params={compose().attrs(["id", "title"]).text().build()}>Button</button>`}>
					<button id="test" title="Title" data-action="open-normal-link" data-params={compose().attrs(["id", "title"]).text().build()}>Button with simple object</button>
				</Example>
				<Example code={`<button data-action="open-normal-link" data-params={compose().values({ id: "this-id", checked: true }).build()}>Button</button>`}>
					<button data-action="open-normal-link" data-params={compose().values({ id: "this-id", checked: true }).build()}>Button with simple values</button>
				</Example>
				<Example code={`<button data-action="open-normal-link" data-params={compose().values({ complex: complex }).build()}>Button</button>`}>
					<button data-action="open-normal-link" data-params={compose().values({ complex: complex }).build()}>Button with complex values</button>
				</Example>
			</div>

			<h3>Zones</h3>
			<p>Zone can be managed in way of zone appear and disappear from page. Telemetry is not able to work with zones that are only hidden by <u>visibility change</u> or <u>display "none"</u>. Here you can show zones and look on console to understand zones features.</p>
			<div className={css.PlainPageList}>
				{zone1Show && (
					<div className={css.PageZone} data-zone="plain-zone-1" data-params="This is zone 1">
						<p>This is content of zone 1. As you can see in browser console, zone is reported as that appear. There are some button with same action <Action action="open-normal-link" /> as above, but these buttons are inside zone so can be reported in nested zone! This zone also trigger synthetic telemetry that tells how long you have stay in zone. Use hide button and look into console!</p>

						<div className={css.PlainPageList}>
							<Example code={`<a href="http://some-url" data-action="open-normal-link" data-params="[attr=href][val:s:value='Cool link']">Simple link</a>`}>
								<Link to={path} data-action="open-normal-link" data-params="[attr=href][val:s:value='Cool link']">Simple link</Link>
							</Example>
							<Example code={`<button id="test" title="Title" data-action="open-normal-link" data-params="[attr=id,title][text]">Button</button>`}>
								<button id="test" title="Title" data-action="open-normal-link" data-params="[attr=id,title][text]">Button</button>
							</Example>
						</div>

						<button data-action="hide-zone" onClick={() => showZone1(!zone1Show)}>Hide zone 1</button>
					</div>
				)}
				{!zone1Show && (
					<Example code={`<div data-zone="plain-zone-1" data-params="This is zone 1">...</div>`}>
						<button data-action="show-zone" onClick={() => showZone1(true)} disabled={zone1Show}>Show zone 1</button>
					</Example>
				)}
				{zone2Show && (
					<div className={css.PageZone} data-zone="plain-zone-2" data-params="[attr=class]">
						<p>This is content of zone 2. As you can see in browser console, zone is reported as that appear. This zone contains another zone that is appear in some time as this zone. So as is showed, you are able to nested zones inside another zones!</p>
						<div className={css.PageZone} data-zone="plain-zone-2-inner" data-params="[attr=class][text]">
							<p>This is content of zone 2 inner :)</p>
						</div>

						<button data-action="hide-zone" onClick={() => showZone2(!zone2Show)}>Hide zone 2</button>
					</div>
				)}
				{!zone2Show && (
					<Example code={`<div data-zone="plain-zone-2" data-params="[attr=class]"><div data-zone="plain-zone-2-inner" data-params="[attr=class][text]">...</div></div>`}>
						<button data-action="show-zone" onClick={() => showZone2(true)} disabled={zone2Show}>Show zone 2</button>
					</Example>
				)}
			</div>

			<h3>Request</h3>
			<p>Request can be also part of telemetry and it is possible to create telemetry on them. Clicking on these buttons you can see spawned <Action action="request-fail" /> or <Action action="request-ok" /> actions. There are also synthetics telemetry actions <Action action="request-success-duration" /> and <Action action="request-fail-duration" /> that are composed from click action and success or fail request action.</p>
			<div className={css.PlainPageList}>
				<Example code={`<button data-action="open-external-link" onClick={() => runFetch("http://www.dsfsdfdsfsdf.cz")} />`}>
					<button data-action="open-external-link" onClick={() => runFetch("http://www.dsfsdfdsfsdf.cz")}>Invalid request by using "fetch"</button>
				</Example>
				<Example code={`<button data-action="open-external-link" onClick={() => runXmlHttpRequest("GET", "http://www.dsfsdfdsfsdf.cz")} />`}>
					<button data-action="open-external-link" onClick={() => runXmlHttpRequest("GET", "http://www.dsfsdfdsfsdf.cz")}>Invalid request by using "XmlHttpRequest"</button>
				</Example>
				<Example code={`<button data-action="open-external-link" onClick={() => runFetch("http://www.google.cz")} />`}>
					<button data-action="open-external-link" onClick={() => runFetch(window.location.origin)}>Valid request by using "fetch"</button>
				</Example>
				<Example code={`<button data-action="open-external-link" onClick={() => runXmlHttpRequest("GET", "http://www.google.cz")} />`}>
					<button data-action="open-external-link" onClick={() => runXmlHttpRequest("GET", window.location.origin)}>Valid request by using "XmlHttpRequest"</button>
				</Example>
			</div>

		</div>
	);
}

function runFetch(url: RequestInfo, init?: RequestInit) {
	return fetch(url, init);
}

function runXmlHttpRequest(method: string, url: string, body?: Document | XMLHttpRequestBodyInit | null) {
	const req = new XMLHttpRequest();
	req.open(method, url);
	req.send(body);
}