import "console.style";


export function reportTelemetry(start: number, end: number, type: string, url: string, telemetry: string | null, zones: Array<string> | null, action: string | null, params: Object | null) {
	const itemsTelemetry = frameworkType(type);
	telemetryName(itemsTelemetry, telemetry);
	telemetryStart(itemsTelemetry, start);
	telemetryZones(itemsTelemetry, zones);
	telemetryAction(itemsTelemetry, action);
	telemetryDuration(itemsTelemetry, start, end);
	telemetryUrl(itemsTelemetry, url);
	itemsTelemetry.length > 0 && console["style"](itemsTelemetry.join(" "));

	const itemsParams = telemetryParams(params);
	itemsParams.length > 0 && console["style"](new Array(type.length + 3).join(" ") + itemsParams.join(" "));
}

export function reportInit(type: string, tracker?: string) {
	const items = frameworkType(type);
	items.push(`<i="color: grey;">Telemetry was initialized</i>`);
	if (tracker) {
		items.push(`<i="color: green;">with tracker</i> <b="color: orange;">${tracker}</b>`);
	} else {
		items.push(`<i="color: green;">with tracker</i> <b>noop</b>`);
	}
	console["style"](items.join(" "));
}

export function reportDispose(type: string) {
	const items = frameworkType(type);
	items.push(`<i="color: red;">Telemetry was dispose.</i>`);
	console["style"](items.join(" "));
}

function frameworkType(type: string): Array<string> {
	return [`<css="font-style: italic;font-weight: bold;">${type}</css>:`];
}

function telemetryName(items: Array<string>, telemetry: string | null) {
	if (telemetry) {
		items.push(`<b>[</b>${telemetry}<b>]</b>`);
	}
}

function telemetryStart(items: Array<string>, start: number) {
	if (start) {
		items.push(`(<css='font-family: "Courier New"'>${new Date(start).toLocaleTimeString()}</css>)`);
	}
}

function telemetryDuration(items: Array<string>, start: number, end: number) {
	if (start && end && end - start > 0) {
		items.push(`<b="color: orange;">duration</b>: <i="color: white;">${end - start}ms</i>`);
	}
}

function telemetryZones(items: Array<string>, zones: Array<string> | null) {
	if (zones && zones.length > 0) {
		items.push(`<b="color: red;">zones</b>: <i>${zones.join(" / ")}</i>`);
	}
}

function telemetryAction(items: Array<string>, action: string | null) {
	if (action) {
		items.push(`<b="color: green;">action</b>: <i>${action}</i>`);
	}
}

function telemetryParams(params: Object | null): Array<string> {
	const items: Array<string> = [];

	if (params && Object.keys(params).length) {
		items.push(` <b>=></b> <b="color: yellow;">parameters</b>: <b>{</b> `);
		Object.keys(params).forEach((key) => {
			if (params[key] !== undefined && typeof params[key] !== "undefined") {
				items.push(`<css="color: gray;">${key}</css>: <i="color: white;">${JSON.stringify(params[key])}</i>, `);
			} else if (params[key] !== undefined) {
				items.push(`<css="color: gray;">${key}</css>: "<i="color: white;">${params[key]}</i>", `);
			}
		});
		items.push(`<b>}</b>`);
	}
	return items;
}

function telemetryUrl(items: Array<string>, url: string) {
	if (url) {
		const color = "rgba(208,208,208,0.4)";
		items.push(`| <i="color: ${color};">on ${url}</i>`);
	}
}