import React, {useState} from "react";
import { useHistory } from "react-router-dom";
import cx from "classnames";

import css from "./main.css";
import {Paths} from "../routes";

export const Main: React.FC = () => {
	return (
		<div className={css.MainPage}>
			<h1>@cuaatt/core</h1>
			<p>
				Module <code>@cuaatt/core</code> is main core module for <b>composable user acceptance and telemetry
				tracking</b> library. This library is using for collecting telemetry and user behaviour from page
				without big impact in application code. Telemetry can be composed of trigger event and have this logic
				on one place in your app. Module can be used separately, but it's included
				inside <code>@cuaatt/core</code> library. So if you want to use <b>react</b> in your app,
				use <code>@cuaatt/react</code>.
			</p>

			<h3>Select library type</h3>
			<p>
				There are examples of usage telemetry <code>@cuaatt/core</code>. Telemetry can now work in <b>plain</b> and <b>react</b>. Other types can be implemented in future.
				Choose what version of telemetry you want to use and look on the examples and how it works!
			</p>

			<div className={css.LibraryType}>
				<LibraryItem title="Plain" text="VanillaJS and core implementation of telemetry for everyone." link={Paths["/plain"]} />
				<LibraryItem title="React" text="React telemetry implementation with providers and helpers used in React apps." link={Paths["/react"]} />
			</div>

			<h3>Select tracker type</h3>
			<p>
				Telemetry <code>@cuaatt/core</code> needs handler that is used for sending telemetry messages. In this handler you can
				do what you need. In most cases these messages are processed and send to telemetry service. You can implemented by
				itself or use already created trackers.
			</p>

			<div className={css.LibraryType}>
				<LibraryItem title="Plain + Google Analytics" text="Tracker for sending events into Google Analytics service used in Plain telemetry." link={Paths["/plain/:tracker"].replace(":tracker", "gtag")} />
				<LibraryItem title="React + Google Analytics" text="Tracker for sending events into Google Analytics service used in React telemetry." link={Paths["/react/:tracker"].replace(":tracker", "gtag")} />
			</div>
		</div>
	);
}

const LibraryItem: React.FC<{ title: string, text: string, link: string}> = ({ title, text , link}) => {
	const [hover, setHover] = useState(false);
	const history = useHistory();

	return (
		<div className={cx(css.LibraryItem, hover && css.LibraryItemHover)}
			 onClick={() => history.push(link)}
			 onMouseOver={() => setHover(true)}
			 onMouseLeave={() => setHover(false)}>
			<b>{title}</b>
			<span>{text}</span>
		</div>
	);
}