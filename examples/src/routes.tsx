

export const Paths = {
	"/": "/",
	"/plain": "/plain",
	"/react": "/react",
	"/react/:tracker": "/react/:tracker",
	"/plain/:tracker": "/plain/:tracker",
}