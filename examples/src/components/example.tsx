import * as React from "react";

import css from "./example.css";

export const Example: React.FC<{ code: string }> = ({ children , code }) => {
	return (
		<div className={css.Example}>
			<div>{children}</div>
			<code>{code}</code>
		</div>
	);
}