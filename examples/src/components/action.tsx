import * as React from "react";

export const Action: React.FC<{ action: string }> = ({ action }) => {
	return (
		<span><b>"</b><u>{action}</u><b>"</b></span>
	);
}