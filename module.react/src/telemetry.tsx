import React from "react";
import {useMemo, useEffect} from "react";
import telemetryInit, {TelemetryOptions, Telemetry, TelemetryHandler} from "@cuaatt/core";
import {compose} from "@cuaatt/params";

export type TelemetryReact<T, Z, A, M> = {
	TelemetryProvider: TelemetryProviderComponent<T, Z, A, M>;
	Zone: TelemetryZoneComponent<Z>;
	Action: TelemetryActionComponent<A>;
}

export function createTelemetry<T, Z, A, M>(): TelemetryReact<T, Z, A, M> {
	const TelemetryContext = createTelemetryContext<T, Z, A, M>();
	const TelemetryProvider = createTelemetryProvider<T, Z, A, M>(TelemetryContext);
	const Zone = createTelemetryZone<T, Z, A, M>(TelemetryContext);
	const Action = createTelemetryAction<T, Z, A, M>(TelemetryContext);

	return {
		TelemetryProvider,
		Zone, Action,
	};
}

function createTelemetryContext<T, Z, A, M>(): React.Context<Telemetry<T, Z, A, M> | null> {
	return React.createContext<Telemetry<T, Z, A, M> | null>(null);
}

export type TelemetryProviderComponent<T, Z, A, M> = React.FC<{ handler: TelemetryHandler<T, Z, A, M>, options?: Partial<TelemetryOptions<T, Z, A, M>> }>;
function createTelemetryProvider<T, Z, A, M>(context: React.Context<Telemetry<T, Z, A, M> | null>): TelemetryProviderComponent<T, Z, A, M> {
	return ({ children, options, handler }) => {
		const opts = options || {};
		const telemetry = useMemo<Telemetry<T, Z, A, M>>(() => {
			return telemetryInit<Z, A, M, T>(handler, (defaults) => {
				return {...defaults, ...opts};
			});
		}, [JSON.stringify(opts)]);

		useEffect(() => () => telemetry.dispose(), [telemetry]);

		return (
			<context.Provider value={telemetry}>{children}</context.Provider>
		);
	}
}

export type TelemetryZoneComponent<Z> = React.FC<{
	name: Z;
	zi?: number;
	pi?: number;
	el: React.ReactElement;
} & TelemetryParams>;
function createTelemetryZone<T, Z, A, M>(context: React.Context<Telemetry<T, Z, A, M> | null>): TelemetryZoneComponent<Z> {
	return ({ children, name, zi, pi, el, value, params, attributes, text}) => {
		return (
			<context.Consumer>
				{(tel) => {

					if (!tel) {
						throw new Error(`Telemetry is not initialized. Use <TelemetryProvider></TelemetryProvider> to initialize it.`);
					}

					const options = tel.options();
					return React.cloneElement(
						el,
						{
							[options.zoneAttributes[zi || 0]]: name,
							[options.paramsAttributes[pi || 0]]: createParams({ value, params, attributes, text })
						},
						children
					);
				}}
			</context.Consumer>
		)
	}
}

export type TelemetryActionComponent<A> = React.FC<{
	name: A;
	ai?: number;
	pi?: number;
	el: React.ReactElement;
} & TelemetryParams>;
function createTelemetryAction<T, Z, A, M>(context: React.Context<Telemetry<T, Z, A, M> | null>): TelemetryActionComponent<A> {
	return ({ children, name, ai, pi, el, value, params, attributes, text}) => {
		return (
			<context.Consumer>
				{(tel) => {

					if (!tel) {
						throw new Error(`Telemetry is not initialized. Use <TelemetryProvider></TelemetryProvider> to initialize it.`);
					}

					const options = tel.options();
					return React.cloneElement(
						el,
						{
							[options.actionAttributes[ai || 0]]: name,
							[options.paramsAttributes[pi || 0]]: createParams({ value, params, attributes, text })
						},
						children
					);
				}}
			</context.Consumer>
		)
	}
}

type TelemetryParams = {
	attributes?: Array<string>;
	text?: boolean;
	value?: string;
	params?: { [key: string]: Object };
}

function createParams({ value, params, attributes, text }: TelemetryParams): string | undefined {
	const comp = compose();

	attributes && comp.attrs(attributes);
	text && comp.text();
	value && comp.value(value);
	params && comp.values(params);

	return comp.build() || undefined;
}