import { TelemetryOptions, Telemetry, TelemetryMessage, TelemetryResolve } from "@cuaatt/core";
import { createTelemetry, TelemetryReact, TelemetryZoneComponent, TelemetryActionComponent } from "./telemetry";

export {
	createTelemetry,
	TelemetryReact,
	TelemetryZoneComponent,
	TelemetryActionComponent,
	//from core
	TelemetryOptions,
	Telemetry,
	TelemetryMessage,
	TelemetryResolve
}