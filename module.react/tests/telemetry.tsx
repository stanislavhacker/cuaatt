import React from "react";
import {createTelemetry, TelemetryReactShortcut, renderComponent, triggerEvent, selectElement, getCalls, checkCall, checkDuration, DurationType} from "./helpers";
import {TelemetryOptionsEmpty} from "./helpers";
import {TelemetryResolve} from "@cuaatt/core";

describe("events telemetry", () => {

	describe("empty settings", () => {
		let tel: TelemetryReactShortcut;
		let handler;

		beforeEach(() => {
			handler = jasmine.createSpy("handler");
			tel = createTelemetry();
		});

		describe("zones", () => {

			it("create empty div zone", async () => {
				const [element, clean] = await renderComponent(
					<tel.TelemetryProvider options={TelemetryOptionsEmpty} handler={handler}>
						<tel.Zone name="main" el={<div />}>Content</tel.Zone>
					</tel.TelemetryProvider>
				);
				expect(element.innerHTML).toBe('<div data-tl-zone="main">Content</div>');
				clean();
			});

			it("create div zone with div element attributes", async () => {
				const [element, clean] = await renderComponent(
					<tel.TelemetryProvider options={TelemetryOptionsEmpty} handler={handler}>
						<tel.Zone name="main" el={<div className="div" title="Title" />}>Content</tel.Zone>
					</tel.TelemetryProvider>
				);
				expect(element.innerHTML).toBe('<div class="div" title="Title" data-tl-zone="main">Content</div>');
				clean();
			});

			it("create div zone with div element attributes and params", async () => {
				const [element, clean] = await renderComponent(
					<tel.TelemetryProvider options={TelemetryOptionsEmpty} handler={handler}>
						<tel.Zone name="main" el={<div className="div" title="Title" />} attributes={["title", "class"]} text value={"value"} params={{ valid: true, count: 22 }}>Content</tel.Zone>
					</tel.TelemetryProvider>
				);
				expect(element.innerHTML).toBe('<div class="div" title="Title" data-tl-zone="main" data-tl-params="[attr=title,class][val:b:valid=\'true\'][val:n:count=\'22\'][text][val:s:value=\'value\']">Content</div>');
				clean();
			});

		});

		describe("actions", () => {

			it("create empty button action", async () => {
				const [element, clean] = await renderComponent(
					<tel.TelemetryProvider options={TelemetryOptionsEmpty} handler={handler}>
						<tel.Action name="button-click" el={<button />}>Button</tel.Action>
					</tel.TelemetryProvider>
				);
				expect(element.innerHTML).toBe('<button data-tl-action="button-click">Button</button>');
				clean();
			});

			it("create button action with button element attributes", async () => {
				const [element, clean] = await renderComponent(
					<tel.TelemetryProvider options={TelemetryOptionsEmpty} handler={handler}>
						<tel.Action name="button-click" el={<button title="Title" name="Name of button" />}>Button</tel.Action>
					</tel.TelemetryProvider>
				);
				expect(element.innerHTML).toBe('<button title="Title" name="Name of button" data-tl-action="button-click">Button</button>');
				clean();
			});

			it("create button action with button element attributes and params", async () => {
				const [element, clean] = await renderComponent(
					<tel.TelemetryProvider options={TelemetryOptionsEmpty} handler={handler}>
						<tel.Action name="button-click" el={<button title="Title" name="Name of button" />} attributes={["title", "name"]} text value={"value"} params={{ valid: true, count: 22 }}>Button</tel.Action>
					</tel.TelemetryProvider>
				);
				expect(element.innerHTML).toBe('<button title="Title" name="Name of button" data-tl-action="button-click" data-tl-params="[attr=title,name][val:b:valid=\'true\'][val:n:count=\'22\'][text][val:s:value=\'value\']">Button</button>');
				clean();
			});

		});

		describe("telemetry", () => {

			it("create zone and action and trigger telemetry", async () => {
				const [element, clean] = await renderComponent(
					<tel.TelemetryProvider options={TelemetryOptionsEmpty} handler={handler}>
						<tel.Zone name="main" el={<div />}>
							<tel.Action name="button-click" el={<button />}>Button</tel.Action>
						</tel.Zone>
					</tel.TelemetryProvider>
				);

				await triggerEvent(await selectElement(element, "button"), "click");

				const calls = await getCalls(handler);
				expect(calls.length).toBe(1);

				checkCall(calls[0], TelemetryResolve.Global, ["main"], "button-click", "button-click");
				checkDuration(calls[0], DurationType.Instant);

				clean();
			});

		});

	});

});