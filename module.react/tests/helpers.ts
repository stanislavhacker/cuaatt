import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import {TelemetryMessage, TelemetryResolve} from "@cuaatt/core";
import {Params} from "@cuaatt/params";
import {createTelemetry as createTelemetryReact, TelemetryOptions, TelemetryReact} from "../src";

export const TelemetryEvents = ["mousedown", "click", "keydown", "keypress", "focus"] as const;

export type Zones = "main" | "navigation" | "content" | "quote" | "menu";
export type Actions = "link-click" | "button-click" | "menu-click";
export type Telemetry = Actions | "menu-opened" | "zone-appear" | "zone-disappear" | "zone-duration" | "request-fail" | "request-ok" | "request-success-duration" | "request-fail-duration";

export const TelemetryOptionsEmpty: OptionsShortcut = {
	events: TelemetryEvents
};

type OptionsShortcut = TelemetryOptions<typeof TelemetryEvents[number], Zones, Actions, Telemetry>;
export type TelemetryReactShortcut = TelemetryReact<typeof TelemetryEvents[number], Zones, Actions, Telemetry>;
export function createTelemetry(): TelemetryReactShortcut {
	return createTelemetryReact<typeof TelemetryEvents[number], Zones, Actions, Telemetry>();
}

export async function getCalls(handler: jasmine.Spy): Promise<Array<TelemetryMessage<typeof TelemetryEvents[number], Zones, Actions, Telemetry>>> {
	await wait(10);
	return handler.calls.all().map((call) => call.args[0]);
}

export async function selectElement(element: Element | null, selector: string): Promise<Element | null> {
	const el = element && element.querySelector(selector);
	if (!el) {
		throw new Error(`Element with selector "${selector}" not found in dom.`);
	}
	return el;
}

export async function wait(timeout: number): Promise<void> {
	return new Promise((resolve) => setTimeout(resolve, timeout));
}

//TRIGGERS

export async function triggerEvent(element: Element | null, type: string): Promise<void> {
	let event;
	if (window.Event) {
		event = new Event(type, { bubbles: true, cancelable: true });
	} else {
		event = document.createEvent("HTMLEvents");
		event.initEvent(type, true, true);
		event.eventName = type;
	}
	if (element) {
		element.dispatchEvent(event);
	} else {
		throw new Error(`Element is not provided.`);
	}
}

//RENDER

export async function renderComponent(component: React.ReactElement): Promise<[Element, () => boolean]> {
	return new Promise((resolve) => {
		const div = document.createElement("div");
		document.body.appendChild(div);
		render(component, div, () => {
			resolve([div, () => unmountComponentAtNode(div)]);
		});
	});
}


//CHECKS

export function checkCall(call: TelemetryMessage<typeof TelemetryEvents[number], Zones, Actions | Telemetry, Telemetry>,
						  resolve: TelemetryResolve, zones: Array<Zones> | undefined, action: Actions | Telemetry | null, telemetry: Telemetry, params: Params = {}) {
	expect(call).toEqual({
		time: {
			start: jasmine.anything(),
			end: jasmine.anything()
		},
		url: jasmine.anything(),
		resolve,
		...(zones ? { zones } : {}),
		action,
		telemetry,
		params
	});
}

export enum DurationType {
	Instant,
	Long
}
export function checkDuration(call: TelemetryMessage<typeof TelemetryEvents[number], Zones, Actions, Telemetry>,
							  dur: DurationType) {
	const duration = call.time.end - call.time.start;

	switch (dur) {
		case DurationType.Long:
			expect(duration).toBeGreaterThan(0);
			break;
		default:
			expect(duration).toBe(0);
			break;

	}
}