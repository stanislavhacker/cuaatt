@cuaatt/react
======

### What is it?

Module `@cuaatt/react` is React version of telemetry module [`@cuaatt/core`][core] for **composable user acceptance and telemetry tracking** library. This library is using for collecting telemetry and user behaviour from page without big impact in application code. Telemetry can be composed of trigger event and have this logic on one place in your app.

> **This is only react package and all necessary settings and principles of telemetry are described in [`@cuaatt/core` package][core]. So if you want to start using telemetry, try to read this documentation first and then continue here.**

### Install

Do with [npm](https://npmjs.org):

```
npm install @cuaatt/react
```

### How to run demo?

1. Download or clone this repo
2. Use `npm install` or `yarn install`
3. Use `npm run make-install`
4. Use `npm run preview`
5. Use generated url and open it in browser

### How to initialize telemetry?

For initializing and working with react version of telemetry, you need to initialize it and that use it in your app. On start, you need to create **telemetry.tsx** that contains options, handler and also export of telemetry provider and zone and action component. Using this is very simple and you can see this on example below.

File: **telemetry.tsx**
```typescript
import React, {useEffect} from "react";
import {createTelemetry, TelemetryZoneComponent, TelemetryActionComponent, TelemetryMessage, TelemetryResolve } from "@cuaatt/react";

const Events = ["mousedown", "mouseup", "click"] as const;
type Action = "action";
type Zones = "zone";
type Telemetry = Action | "telemetry";

const tel = createTelemetry<typeof Events[number], Zones, Action, Telemetry>();

export const Zone: TelemetryZoneComponent<Zones> = tel.Zone;
export const Action: TelemetryActionComponent<Action> = tel.Action;

export const Telemetry: React.FC<> = ({ children }) => {
	return (
		//options = options for telemetry
		//handler = callback function fo handling telemetry messages 
		<tel.TelemetryProvider 
            options={{...}} 
            handler={handle}
        >{children}</tel.TelemetryProvider>
	)
};

function handle(info: TelemetryMessage<typeof Events[number], Zones, Action, Telemetry | Action>) {
    //handle messahges
}
```

File: **app.tsx**
```typescript
import * as React from "react";
import {Telemetry, Zone, Action} from "./telemetry";

export const App: React.FC = () => {
	return (
		<Telemetry tracker={tracker}>
			<Zone name="zone" el={<div className="className" id="zone1" />} attributes={["id"]} params={{ valid: true, user: "Stanley" }}>
				<Action name="action" attributes={["href"]} params={{ value: "Cool link" }} text el={<a href={path} target="_blank" />}>Simple link</Action>
            </Zone>
		</Telemetry>
	);
}
```

### Zone

Zones are part of page that are some logically different from another. For example different zones can be modal dialogs, some subpage or some floating menu. Basically it can be any element on page. In react version, zones are components that has children and required el property when you can specify element with attributes and Zone component added all necessary data attributes and params on it.

**Example of usage for define zone**

```typescript
function Component() {
	return <Zone name="zone" attributes={["title", "id"]} params={{ valid: true }} text el={<div title="Menu" id="menu1" className="menu" />}>...</Zone>;
}
```

On zone can be specified these properties.

- **`name: Z;`**

This is name of zone and is required.

- **`el: React.ReactElement;`**

This is template of element that will be used for zone. Zone component automatically adds required data attributes for zone and params.

- **`attributes: Array<string>;`**

Array of all attributes that will be loaded and propagated into telemetry messages as parameters.

- **`params: { [key: string]: any };`**

Object with key value items, that will be loaded and propagated into telemetry messages as parameters.

- **`text: boolean;`**

When you specified text on true, telemetry will load text content of zone into parameters.

### Action

Actions are trigger by user and can be on buttons, links, spans and other elements. In react version, actions are components that has children and required el property when you can specify element with attributes and Action component added all necessary data attributes and params on it.

**Example of usage for define action**

```typescript
function Component() {
    return <Action name="zone" attributes={["title", "id"]} params={{ valid: true }} text el={<button title="Menu" id="menu1" className="menu" onClick={() => {}} />}>...</Action>;
}
```

On action can be specified these properties.

- **`name: A;`**

This is name of action and is required.

- **`el: React.ReactElement;`**

This is template of element that will be used for action. Action component automatically adds required data attributes for action and params.

- **`attributes: Array<string>;`**

Array of all attributes that will be loaded and propagated into telemetry messages as parameters.

- **`params: { [key: string]: any };`**

Object with key value items, that will be loaded and propagated into telemetry messages as parameters.

- **`text: boolean;`**

When you specified text on true, telemetry will load text content of action into parameters.


### Donate me 😉

| QR | Paypal |
| ------ | ------ |
| ![](https://gitlab.com/uploads/-/system/personal_snippet/1929487/66399a49a06fa8eb9a0758b8673758c5/qr_sh.png) | [![](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DUT8W343NVGQQ&source=url) |


### License

MIT - [MIT License](https://spdx.org/licenses/MIT.html)

[core]: https://gitlab.com/stanislavhacker/cuaatt/-/blob/master/module.core/README.md